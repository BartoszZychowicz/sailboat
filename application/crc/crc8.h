/**
 ******************************************************************************
 * @file    platCmn.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   CRC8 header file.
 ******************************************************************************
 */

#ifndef CRC_CRC8_H_
#define CRC_CRC8_H_

uint8_t CRC8_calcChecksum( const uint8_t * data, size_t size );

#endif /* CRC_CRC8_H_ */
