/**
 ******************************************************************************
 * @file    debug.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Header file of debug.
 ******************************************************************************
 */

#ifndef DEBUG_DEBUG_H_
#define DEBUG_DEBUG_H_

#ifdef DEBUG

void DEBUG_initialize( void );

void vTaskDebug( void *p );

TaskHandle_t* DEBUG_getTaskHndl( void );

#else

#define DEBUG_initialize()

#define vTaskDebug()

#define DEBUG_getTaskHndl() 0

#endif

#endif /* DEBUG_DEBUG_H_ */
