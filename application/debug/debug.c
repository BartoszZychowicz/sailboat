/**
 ******************************************************************************
 * @file    debug.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Debug menu and functionalities.
 ******************************************************************************
 */

#ifdef DEBUG

//========INCLUDES========

#include "appHeaders.h"

//========DEFINES========

#define DBGOUT(...)   DBG_LOCK(); \
                      printf(__VA_ARGS__);  \
                      DBG_UNLOCK()

#define SHOW_CURRENT_MENU_INFO() menus[dbgCtrl.menu].info()

#define DEBUG_INPUT_BUFFER_SIZE 150

//========TYPES========

typedef enum
{
  DEBUG_MENU_TYPE_MAIN,
  DEBUG_MENU_TYPE_MODULES,
  DEBUG_MENU_TYPE_MPU9250,
  DEBUG_MENU_TYPE_TEST,
  DEBUG_MENU_TYPE_MAX
} T_DEBUG_MENU_TYPE;

typedef struct
{
  uint8_t inputBuf[DEBUG_INPUT_BUFFER_SIZE];
  T_DEBUG_MENU_TYPE menu;
  TaskHandle_t taskHndl;
} T_DEBUG_CTRL;

typedef struct
{
  void (*run)( uint8_t );
  void (*info)( void );
} T_DEBUG_MENU;

//========STATIC FUNCTIONS PROTOTYPES========

static void DEBUG_initDebugTask( void );

static void DEBUG_mainMenu( uint8_t cmd );

static void DEBUG_mainMenuInfo( void );

static void DEBUG_modulesMenu( uint8_t cmd );

static void DEBUG_modulesMenuInfo( void );

static void DEBUG_MPU9250Menu( uint8_t cmd );

static void DEBUG_MPU9250MenuInfo( void );

static void DEBUG_testMenu( uint8_t cmd );

static void DEBUG_testMenuInfo( void );

static void DEBUG_printTasksStack( void );

static float DEBUG_getFloatParam( void );

static uint32_t DEBUG_getIntParam( void );

static void DEBUG_testGetData( void );

static void DEBUG_goToMenu( T_DEBUG_MENU_TYPE dest );


//========STATIC VARIABLES========

static T_DEBUG_CTRL dbgCtrl;

static const T_DEBUG_MENU menus[DEBUG_MENU_TYPE_MAX] =
{
  [DEBUG_MENU_TYPE_MAIN].run      = &DEBUG_mainMenu,
  [DEBUG_MENU_TYPE_MAIN].info     = &DEBUG_mainMenuInfo,
  [DEBUG_MENU_TYPE_MPU9250].run   = &DEBUG_MPU9250Menu,
  [DEBUG_MENU_TYPE_MPU9250].info  = &DEBUG_MPU9250MenuInfo,
  [DEBUG_MENU_TYPE_MODULES].run   = &DEBUG_modulesMenu,
  [DEBUG_MENU_TYPE_MODULES].info  = &DEBUG_modulesMenuInfo,
  [DEBUG_MENU_TYPE_TEST].run      = &DEBUG_testMenu,
  [DEBUG_MENU_TYPE_TEST].info     = &DEBUG_testMenuInfo,
};

//========STATIC FUNCTIONS DEFINITIONS========

static void DEBUG_initDebugTask( void )
{
  xTaskCreate( vTaskDebug, (const char*) "DebugTask", PLATCMN_STACK_SIZE_DEBUG, NULL, 1, &dbgCtrl.taskHndl );
}

static void DEBUG_mainMenu( uint8_t cmd )
{
  switch( cmd )
  {
    case 'i':
      SHOW_CURRENT_MENU_INFO();
      break;

    case 'm':
      DEBUG_goToMenu( DEBUG_MENU_TYPE_MODULES );
      break;

    case 'p':
      DEBUG_goToMenu( DEBUG_MENU_TYPE_MPU9250 );
      break;

    case '1':
      DEBUG_goToMenu( DEBUG_MENU_TYPE_TEST );
      break;

    default:
      break;
  }
}

static void DEBUG_mainMenuInfo( void )
{
  DBGOUT("=== Main menu ===\n\n"
         "i - info\n"
         "m - modules menu\n"
         "p - MPU9250 menu\n"
         "1 - test menu\n\n");
}

static void DEBUG_modulesMenu( uint8_t cmd )
{
  switch( cmd )
    {
      case 'i':
        SHOW_CURRENT_MENU_INFO();
        break;

      case 'E':
        ENCODER_initialize();
        break;

      case 'e':
        ENCODER_deactivate();
        break;

      case 'G':
        GPS_initialize();
        break;

      case 'g':
        GPS_deactivate();
        break;

      case 'M':
        MPU9250_initialize();
        break;

      case 'm':
        MPU9250_deactivate();
        break;

      case PLATCMN_ASCII_ESC:
        DEBUG_goToMenu( DEBUG_MENU_TYPE_MAIN );
        break;

      default:
        break;
    }
}

static void DEBUG_modulesMenuInfo( void )
{
  DBGOUT("=== Modules menu ===\n\n"
         "i - info\n"
         "E - activate Encoder\n"
         "e - deactivate Encoder\n"
         "G - activate GPS\n"
         "g - deactivate GPS\n"
         "M - activate MPU9250\n"
         "m - deactivate MPU9250\n"
         "ESC - back\n\n");
}


static void DEBUG_MPU9250Menu( uint8_t cmd )
{
  float temp = 0;
  switch( cmd )
  {
    case 'b':
      temp = DEBUG_getFloatParam();
      MPU9250_setBeta(temp);
      break;

    case 'i':
      SHOW_CURRENT_MENU_INFO();
      break;
#ifndef USE_X_IO_MADGWICK
    case 'z':
      temp = DEBUG_getFloatParam();
      MPU9250_setZeta(temp);
      break;
#endif
    case 'w':
      MPU9250_setQuaternions();
      break;

    case PLATCMN_ASCII_ESC:
      DEBUG_goToMenu( DEBUG_MENU_TYPE_MAIN );;
      break;

    default:
      break;
  }
}

static void DEBUG_MPU9250MenuInfo( void )
{
  DBGOUT("=== MPU9250 Menu ===\n\n"
         "i - info\n"
         "b - set beta parameter\n"
#ifndef USE_X_IO_MADGWICK
         "z - set zeta parameter\n"
#endif
         "w - set wrong initial position\n"
         "ESC - back\n\n");
}

static void DEBUG_testMenu( uint8_t cmd )
{
  float temp = 0;
  switch( cmd )
  {
    case 'a':
      DATAMGMT_triggerDataRead();
      break;

    case 'i':
      SHOW_CURRENT_MENU_INFO();
      break;

    case 'R':
      DEBUG_printTasksStack();
      break;

    case 't':
      DEBUG_testGetData();
      break;

    case 'p':
      GPS_parseSentence( (uint8_t*)&dbgCtrl.inputBuf );
      break;

    case '1':
      temp = DEBUG_getFloatParam();
      PWM_setCompValue( PWM_CHNL_TIM4_CHNL3, (uint32_t)temp );
      break;

    case '2':
      temp = DEBUG_getFloatParam();
      PWM_setCompValue( PWM_CHNL_TIM4_CHNL4, (uint32_t)temp );
      break;

    case PLATCMN_ASCII_ESC:
      DEBUG_goToMenu( DEBUG_MENU_TYPE_MAIN );;
      break;

    default:
      break;
  }
}

static void DEBUG_testMenuInfo( void )
{
  DBGOUT("=== Test menu ===\n\n"
         "i - info\n"
         "a - read data and output to ctrl\n"
         "R - print stack watermarks\n"
         "t - test input(max %d chars)\n"
         "p - parse GPS sentence from buf\n"
         "1 - set PWM channel 3\n"
         "2 - set PWM channel 4\n"
         "ESC - back\n\n", DEBUG_INPUT_BUFFER_SIZE);
}

static void DEBUG_printTasksStack( void )
{
  BaseType_t watermark = 0;
  size_t  heapSize = 0;

  watermark = uxTaskGetStackHighWaterMark( *( I2C_getTask1Hndl() ) );
  DBGOUT( "\nI2C:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_I2C1 );

  watermark = uxTaskGetStackHighWaterMark( *( MPU9250_getTaskHndl() ) );
  DBGOUT( "MPU:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_MPU9250 );

  watermark = uxTaskGetStackHighWaterMark( *( ENCODER_getTaskHndl() ) );
  DBGOUT( "ENC:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_ENCODER );

  watermark = uxTaskGetStackHighWaterMark( *( GPS_getTaskHndl() ) );
  DBGOUT( "GPS:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_GPS );

  watermark = uxTaskGetStackHighWaterMark( *( DATAMGMT_getTaskHndl() ) );
  DBGOUT( "DTMGMT:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_DATAMGMT );

  watermark = uxTaskGetStackHighWaterMark( *( GPIO_getHeartbeatTaskHndl() ) );
  DBGOUT( "HB:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_HEARTBEAT );

  watermark = uxTaskGetStackHighWaterMark( dbgCtrl.taskHndl );
  DBGOUT( "DBG:\t%lu / %u\n", watermark, PLATCMN_STACK_SIZE_DEBUG );

  heapSize = xPortGetFreeHeapSize();
  DBGOUT( "Heap:\t%lu / %u\n", heapSize, configTOTAL_HEAP_SIZE );
}

static float DEBUG_getFloatParam( void )
{
  float result = 0;
  DEBUG_testGetData();
  result = strtof( ( char* )&dbgCtrl.inputBuf, NULL );

  return result;
}

static uint32_t DEBUG_getIntParam( void )
{
  float result = 0;
  char * ptr = NULL;
  DEBUG_testGetData();
  result = (uint32_t)strtol( ( char* )&dbgCtrl.inputBuf, &ptr, 10 );
  DBGOUT("Set PWM val:%d\n\n", result);

  return result;
}

static void DEBUG_testGetData( void ) //TODO: status variable
{
  uint8_t rcvd = 0, i = 0, j = 0;
  DBGOUT( ">\n" );
  while( i < DEBUG_INPUT_BUFFER_SIZE )
  {
    if( UART_getRcvdDataCount( UART_PORT_DBG ) > 0 )
    {
      UART_readData( UART_PORT_DBG, (uint8_t*)&rcvd, 1 );
      switch( rcvd )
      {
        case PLATCMN_ASCII_CR:
          dbgCtrl.inputBuf[i++] = rcvd;
          DBGOUT( "Input: " );
          for( j = 0; j < i; j++ )
          {
            DBGOUT( "%c", dbgCtrl.inputBuf[j] );
          }
          return;

        case PLATCMN_ASCII_ESC:
          DBGOUT( "Cancelled!\n" );
          return;

        default:
          dbgCtrl.inputBuf[i++] = rcvd;
          break;
      }
    }
  }
}

static void DEBUG_goToMenu( T_DEBUG_MENU_TYPE dest )
{
  if( dbgCtrl.menu != dest )
  {
    dbgCtrl.menu = dest;
    SHOW_CURRENT_MENU_INFO();
  }
}

//========EXPORTED FUNCTIONS DEFINITIONS========

void DEBUG_initialize( void )
{
  dbgCtrl.menu = DEBUG_MENU_TYPE_MAIN;
  memset( &dbgCtrl.inputBuf, 0, sizeof( dbgCtrl.inputBuf ) );
  DEBUG_initDebugTask();
  ASSERT( dbgCtrl.taskHndl );
  SHOW_CURRENT_MENU_INFO();
}

void vTaskDebug(void *p)
{
  uint8_t rcvdChar = 0;
  while( 1 )
  {
    if( UART_getRcvdDataCount( UART_PORT_DBG ) > 0 )
    {
      UART_readData( UART_PORT_DBG, (uint8_t*)&rcvdChar, 1 );
      menus[dbgCtrl.menu].run( rcvdChar );
    }
  }
}

TaskHandle_t* DEBUG_getTaskHndl( void )
{
  return ( TaskHandle_t* )dbgCtrl.taskHndl;
}

#endif /* DEBUG */
