/**
 ******************************************************************************
 * @file    MPU9250.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   MPU9250 9DoF sensor interface.
 *          Configuration based on kriswiner's repository:
 *          https://github.com/kriswiner/MPU9250
 ******************************************************************************
 */


//========INCLUDES========

#include "appHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...)   DBG_LOCK(); \
                      printf(__VA_ARGS__);  \
                      DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#define MPU9250_I2C_PORT                  I2C_PORT_1

#define MPU9250_DEV_ADRESS                ( 0x68<<1 ) // !< MPU9250 I2C address.
#define MPU9250_MAG_DEV_ADDRESS           ( 0x0C<<1 ) // !< Magnetometer I2C address in MPU pass-through mode (direct access).

#define MPU9250_WHOAMI_OK                 0x71
#define MPU9250_MAG_WHOAMI_OK             0x48

// MPU9250 register addresses
#define MPU9250_REG_SELF_TEST_X_GYRO      0x00
#define MPU9250_REG_SELF_TEST_Y_GYRO      0x01
#define MPU9250_REG_SELF_TEST_Z_GYRO      0x02

#define MPU9250_REG_SELF_TEST_X_ACCEL     0x0D
#define MPU9250_REG_SELF_TEST_Y_ACCEL     0x0E
#define MPU9250_REG_SELF_TEST_Z_ACCEL     0x0F

#define MPU9250_REG_XG_OFFSET_H           0x13  // User-defined trim values for gyroscope
#define MPU9250_REG_XG_OFFSET_L           0x14
#define MPU9250_REG_YG_OFFSET_H           0x15
#define MPU9250_REG_YG_OFFSET_L           0x16
#define MPU9250_REG_ZG_OFFSET_H           0x17
#define MPU9250_REG_ZG_OFFSET_L           0x18
#define MPU9250_REG_SMPLRT_DIV            0x19
#define MPU9250_REG_CONFIG                0x1A
#define MPU9250_REG_GYRO_CONFIG           0x1B
#define MPU9250_REG_ACCEL_CONFIG          0x1C
#define MPU9250_REG_ACCEL_CONFIG2         0x1D
#define MPU9250_REG_LP_ACCEL_ODR          0x1E
#define MPU9250_REG_WOM_THR               0x1F

#define MPU9250_REG_FIFO_EN               0x23
#define MPU9250_REG_I2C_MST_CTRL          0x24
#define MPU9250_REG_I2C_SLV0_ADDR         0x25
#define MPU9250_REG_I2C_SLV0_REG          0x26
#define MPU9250_REG_I2C_SLV0_CTRL         0x27
#define MPU9250_REG_I2C_SLV1_ADDR         0x28
#define MPU9250_REG_I2C_SLV1_REG          0x29
#define MPU9250_REG_I2C_SLV1_CTRL         0x2A
#define MPU9250_REG_I2C_SLV2_ADDR         0x2B
#define MPU9250_REG_I2C_SLV2_REG          0x2C
#define MPU9250_REG_I2C_SLV2_CTRL         0x2D
#define MPU9250_REG_I2C_SLV3_ADDR         0x2E
#define MPU9250_REG_I2C_SLV3_REG          0x2F
#define MPU9250_REG_I2C_SLV3_CTRL         0x30
#define MPU9250_REG_I2C_SLV4_ADDR         0x31
#define MPU9250_REG_I2C_SLV4_REG          0x32
#define MPU9250_REG_I2C_SLV4_DO           0x33
#define MPU9250_REG_I2C_SLV4_CTRL         0x34
#define MPU9250_REG_I2C_SLV4_DI           0x35
#define MPU9250_REG_I2C_MST_STATUS        0x36
#define MPU9250_REG_INT_PIN_CFG           0x37
#define MPU9250_REG_INT_ENABLE            0x38
#define MPU9250_REG_DMP_INT_STATUS        0x39  // Check DMP interrupt
#define MPU9250_REG_INT_STATUS            0x3A
#define MPU9250_REG_ACCEL_XOUT_H          0x3B
#define MPU9250_REG_ACCEL_XOUT_L          0x3C
#define MPU9250_REG_ACCEL_YOUT_H          0x3D
#define MPU9250_REG_ACCEL_YOUT_L          0x3E
#define MPU9250_REG_ACCEL_ZOUT_H          0x3F
#define MPU9250_REG_ACCEL_ZOUT_L          0x40
#define MPU9250_REG_TEMP_OUT_H            0x41
#define MPU9250_REG_TEMP_OUT_L            0x42
#define MPU9250_REG_GYRO_XOUT_H           0x43
#define MPU9250_REG_GYRO_XOUT_L           0x44
#define MPU9250_REG_GYRO_YOUT_H           0x45
#define MPU9250_REG_GYRO_YOUT_L           0x46
#define MPU9250_REG_GYRO_ZOUT_H           0x47
#define MPU9250_REG_GYRO_ZOUT_L           0x48
#define MPU9250_REG_EXT_SENS_DATA_00      0x49
#define MPU9250_REG_EXT_SENS_DATA_01      0x4A
#define MPU9250_REG_EXT_SENS_DATA_02      0x4B
#define MPU9250_REG_EXT_SENS_DATA_03      0x4C
#define MPU9250_REG_EXT_SENS_DATA_04      0x4D
#define MPU9250_REG_EXT_SENS_DATA_05      0x4E
#define MPU9250_REG_EXT_SENS_DATA_06      0x4F
#define MPU9250_REG_EXT_SENS_DATA_07      0x50
#define MPU9250_REG_EXT_SENS_DATA_08      0x51
#define MPU9250_REG_EXT_SENS_DATA_09      0x52
#define MPU9250_REG_EXT_SENS_DATA_10      0x53
#define MPU9250_REG_EXT_SENS_DATA_11      0x54
#define MPU9250_REG_EXT_SENS_DATA_12      0x55
#define MPU9250_REG_EXT_SENS_DATA_13      0x56
#define MPU9250_REG_EXT_SENS_DATA_14      0x57
#define MPU9250_REG_EXT_SENS_DATA_15      0x58
#define MPU9250_REG_EXT_SENS_DATA_16      0x59
#define MPU9250_REG_EXT_SENS_DATA_17      0x5A
#define MPU9250_REG_EXT_SENS_DATA_18      0x5B
#define MPU9250_REG_EXT_SENS_DATA_19      0x5C
#define MPU9250_REG_EXT_SENS_DATA_20      0x5D
#define MPU9250_REG_EXT_SENS_DATA_21      0x5E
#define MPU9250_REG_EXT_SENS_DATA_22      0x5F
#define MPU9250_REG_EXT_SENS_DATA_23      0x60
#define MPU9250_REG_MOT_DETECT_STATUS     0x61
#define MPU9250_REG_I2C_SLV0_DO           0x63
#define MPU9250_REG_I2C_SLV1_DO           0x64
#define MPU9250_REG_I2C_SLV2_DO           0x65
#define MPU9250_REG_I2C_SLV3_DO           0x66
#define MPU9250_REG_I2C_MST_DELAY_CTRL    0x67
#define MPU9250_REG_SIGNAL_PATH_RESET     0x68
#define MPU9250_REG_MOT_DETECT_CTRL       0x69
#define MPU9250_REG_USER_CTRL             0x6A  // Bit 7 enable DMP, bit 3 reset DMP
#define MPU9250_REG_PWR_MGMT_1            0x6B  // Device defaults to the SLEEP mode
#define MPU9250_REG_PWR_MGMT_2            0x6C
#define MPU9250_REG_DMP_BANK              0x6D  // Activates a specific bank in the DMP
#define MPU9250_REG_DMP_RW_PNT            0x6E  // Set read/write pointer to a specific start address in specified DMP bank
#define MPU9250_REG_DMP_REG               0x6F  // Register in DMP from which to read or to which to write
#define MPU9250_REG_DMP_REG_1             0x70
#define MPU9250_REG_DMP_REG_2             0x71
#define MPU9250_REG_FIFO_COUNTH           0x72
#define MPU9250_REG_FIFO_COUNTL           0x73
#define MPU9250_REG_FIFO_R_W              0x74
#define MPU9250_REG_WHO_AM_I              0x75  // Should return 0x71
#define MPU9250_REG_XA_OFFSET_H           0x77
#define MPU9250_REG_XA_OFFSET_L           0x78
#define MPU9250_REG_YA_OFFSET_H           0x7A
#define MPU9250_REG_YA_OFFSET_L           0x7B
#define MPU9250_REG_ZA_OFFSET_H           0x7D
#define MPU9250_REG_ZA_OFFSET_L           0x7E

// AK8963 magnetometer register addresses
#define MPU9250_REGM_WHO_AM_I             0x00  // should return 0x48
#define MPU9250_REGM_INFO                 0x01
#define MPU9250_REGM_ST1                  0x02  // data ready status bit 0
#define MPU9250_REGM_XOUT_L               0x03  // data
#define MPU9250_REGM_XOUT_H               0x04
#define MPU9250_REGM_YOUT_L               0x05
#define MPU9250_REGM_YOUT_H               0x06
#define MPU9250_REGM_ZOUT_L               0x07
#define MPU9250_REGM_ZOUT_H               0x08
#define MPU9250_REGM_ST2                  0x09  // Data overflow bit 3 and data read error status bit 2
#define MPU9250_REGM_CNTL                 0x0A  // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
#define MPU9250_REGM_ASTC                 0x0C  // Self test control
#define MPU9250_REGM_I2CDIS               0x0F  // I2C disable
#define MPU9250_REGM_ASAX                 0x10  // Fuse ROM x-axis sensitivity adjustment value
#define MPU9250_REGM_ASAY                 0x11  // Fuse ROM y-axis sensitivity adjustment value
#define MPU9250_REGM_ASAZ                 0x12  // Fuse ROM z-axis sensitivity adjustment value

//========TYPES========

// Accel full scale
typedef enum
{
  MPU9250_AFS_2G = 0,
  MPU9250_AFS_4G,
  MPU9250_AFS_8G,
  MPU9250_AFS_16G
} T_MPU9250_AFS;

// Gyro full scale
typedef enum
{
  MPU9250_GFS_250DPS = 0,
  MPU9250_GFS_500DPS,
  MPU9250_GFS_1000DPS,
  MPU9250_GFS_2000DPS
} T_MPU9250_GFS;

// Mag full scale
typedef enum
{
  MPU9250_MFS_14BITS = 0, // 0.6 mG per LSB
  MPU9250_MFS_16BITS      // 0.15 mG per LSB
} T_MPU9250_MFS;

// MPU9250 states
typedef enum
{
  MPU9250_ST_NONE,
  MPU9250_ST_SETUP,
  MP9250_ST_WAITING_DATA_RDY,
  MPU9250_ST_READING_DATA,
  MPU9250_ST_CALCULATING,
  MPU9250_ST_UPDATING_DATA,
  MPU9250_ST_ERROR,
  MPU9250_ST_MAX
} T_MPU9250_ST;

typedef enum
{
  MPU9250_SETUP_ST_NONE,
  MPU9250_SETUP_ST_START,
  MPU9250_SETUP_ST_CHECK_WHOAMI,
  MPU9250_SETUP_ST_RESET,
  MPU9250_SETUP_ST_MPU_CALIB,
  MPU9250_SETUP_ST_MPU_INIT,
  MPU9250_SETUP_ST_CHECK_MAG_WHOAMI,
  MPU9250_SETUP_ST_MAG_INIT,
  MPU9250_SETUP_ST_GET_RES,
  MPU9250_SETUP_ST_TEST_READ,
  MPU9250_SETUP_ST_DONE,
  MPU9250_SETUP_ST_ERROR,
  MPU9250_SETUP_ST_MAX
} T_MPU9250_SETUP_ST;

typedef struct
{
  float     accelRes;
  float     gyroRes;
  float     magRes;

  float     accelBias[3];
  float     gyroBias[3];
  float     magBias[3];   //hardiron correction
  float     magScale[3];  //softiron correction

  float     magCalib[3];

  float     iterationTime;      // !< Time (in s) since last correct data read from MPU
  uint32_t  lastReadTimeStamp;  // !< Time (in ticks ==> ms) at which last correct data read from MPU

  uint8_t       magODR;   // Magnetometer output data rate

  T_MPU9250_AFS accelFS;
  T_MPU9250_GFS gyroFS;
  T_MPU9250_MFS magFS;

  TaskHandle_t taskHndl;
  bool deactivateRequest;

  T_MPU9250_SETUP_ST setupState;
  T_MPU9250_ST  state;

} T_MPU9250_CTRL;

typedef struct
{
  float q[4];
  float beta;
#ifndef USE_X_IO_MADGWICK
  float zeta;
  float flux[2];
  float gyroBiasErr[3];
#endif
  float decl;   // !< magnetic declination
} T_MPU9250_CALC_DATA;

typedef struct
{
  uint32_t timeStamp;   // !< Time (in ticks ==> ms) at which data was read
  int16_t accel[3];
  int16_t gyro[3];
  int16_t mag[3];
} T_MP9250_READINGS;

//========STATIC VARIABLES========

static T_MPU9250_CTRL mpuCtrl;
static T_MP9250_READINGS readings;
static T_MPU9250_OUT_DATA localOutData;
static T_MPU9250_OUT_DATA publicOutData;
static T_MPU9250_CALC_DATA calcData;

//========EXPORTED VARIABLES========

SemaphoreHandle_t MPU9250DataMutex;

//========STATIC FUNCTIONS PROTOTYPES========

static void MPU9250_initMP9250Task( void );

static bool MPU9250_reset( void );

static void MPU9250_setup( void );

static bool MPU9250_initDevice( void );

static bool MPU9250_calibrate( float* gyroDestBias, float* accelDestBias );

static bool MPU9250_initAK8963(float* magDestBias);

static void MPU9250_getSensorResolutions( void );

static bool MPU9250_readAccel( int16_t* data );

static bool MPU9250_readGyro( int16_t* data );

static bool MPU9250_readMag( int16_t* data );

static bool MPU9250_writeReg( uint8_t devAddr, uint8_t regAddr, uint8_t *data );

static bool MPU9250_readData( uint8_t devAddr, uint8_t regAddr, uint8_t size, uint8_t *data );

static void MPU9250_convertReadings( void );

static float MPU9250_calcIterationTime( uint32_t prevReadTime, uint32_t newReadTime );
#ifdef USE_X_IO_MADGWICK
static void MPU9250_madgwickQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);
#else
static void MPU9250_madgwickQuaternionUpdate( float a_x, float a_y, float a_z, float w_x, float w_y, float w_z, float m_x, float m_y, float m_z );
#endif
static void MPU9250_calcAngles( float* q );

static void MPU9250_updatePublicData( void );

static void MPU9250_invalidateOutData( void );

static void MPU9250_setState( T_MPU9250_ST state );

static void MPU9250_setSetupState( T_MPU9250_SETUP_ST state );

//========STATIC FUNCTIONS DEFINITIONS========

static void MPU9250_initMP9250Task( void )
{
  xTaskCreate( vTaskMPU9250, (const char*) "MPU9250Task", PLATCMN_STACK_SIZE_MPU9250, NULL, 1, &mpuCtrl.taskHndl );
  ASSERT( mpuCtrl.taskHndl );
}

static bool MPU9250_reset( void )
{
  uint8_t tempData = 0x80;

  return MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_1, &tempData );
}

static void MPU9250_setup( void )   //TODO: error handling in setup
{
  bool res = false;
  uint8_t data = 0;

  switch(mpuCtrl.setupState)
  {
    case MPU9250_SETUP_ST_START:

      MPU9250_setSetupState( MPU9250_SETUP_ST_CHECK_WHOAMI );
      break;

    case MPU9250_SETUP_ST_CHECK_WHOAMI:

      res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_WHO_AM_I, 1, &data);

      if( res == true && data == MPU9250_WHOAMI_OK )
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_RESET );
      }
      else
      {
        DBGOUT( "MPU9250: Who am I result incorrect: 0x%02x!\n", data );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }
      break;

    case MPU9250_SETUP_ST_RESET:

      res = MPU9250_reset();
      vTaskDelay( 10 );
      if( res == true )
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_MPU_CALIB );
      }
      else
      {
        DBGOUT( "MPU9250: Device reset failed!\n" );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }

      break;

    case MPU9250_SETUP_ST_MPU_CALIB:

      res = MPU9250_calibrate( ( float* )&mpuCtrl.gyroBias, ( float* )&mpuCtrl.accelBias );

      if( res == true )
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_MPU_INIT );
      }
      else
      {
        DBGOUT( "MPU9250: MPU calibration failed!\n" );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }
      break;

    case MPU9250_SETUP_ST_MPU_INIT:

      res = MPU9250_initDevice();

      if(res == true)
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_CHECK_MAG_WHOAMI );
      }
      else
      {
        DBGOUT( "MPU9250: MPU initialization failed!\n" );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }
      break;

    case MPU9250_SETUP_ST_CHECK_MAG_WHOAMI:

      res = MPU9250_readData( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_WHO_AM_I, 1, &data);

      if( res == true && data == MPU9250_MAG_WHOAMI_OK )
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_MAG_INIT );
      }
      else
      {
        DBGOUT( "MPU9250: Magnetometer Who am I result incorrect: 0x%02x!\n", data );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }
      break;

    case MPU9250_SETUP_ST_MAG_INIT:

      res = MPU9250_initAK8963( ( float* )&mpuCtrl.magCalib );

      if( res == true )
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_GET_RES );
      }
      else
      {
        DBGOUT( "MPU9250: Magnetometer initialization failed!\n" );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }
      break;

    case MPU9250_SETUP_ST_GET_RES:

      MPU9250_getSensorResolutions();
      MPU9250_setSetupState( MPU9250_SETUP_ST_TEST_READ );
      break;

    case MPU9250_SETUP_ST_TEST_READ:

      DBGOUT( "MPU9250: Raw data test read:\n" );

      res = MPU9250_readAccel( ( int16_t* )&readings.accel );
      DBGOUT( "ax: %d, ay: %d, az: %d\n", readings.accel[0], readings.accel[1], readings.accel[2] );

      res &= MPU9250_readGyro( ( int16_t* )&readings.gyro );
      DBGOUT( "gx: %d, gy: %d, gz: %d\n", readings.gyro[0], readings.gyro[1], readings.gyro[2] );

      res &= MPU9250_readMag( ( int16_t* )&readings.mag );
      DBGOUT( "mx: %d, my: %d, mz: %d\n", readings.mag[0], readings.mag[1], readings.mag[2] );

      if( res == true)
      {
        MPU9250_setSetupState( MPU9250_SETUP_ST_DONE );
      }
      else
      {
        DBGOUT( "MPU9250: Test data read failed!\n" );
        MPU9250_setSetupState( MPU9250_SETUP_ST_ERROR );
      }
      break;

    default:
      break;
  }
}

// Initialize hardware
static bool MPU9250_initDevice( void )
{
  uint8_t tempData = 0;
  bool res = false;

  // Initialize MPU9250 device
  // wake up device
  tempData = 0x00;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_1, &tempData ); // Clear sleep mode bit (6), enable all sensors
  if( res == false ) return false;
  vTaskDelay( 100 ); // Delay 100 ms for PLL to get established on x-axis gyro; should check for PLL ready interrupt

  tempData = 0x01; // Set clock source to be PLL with x-axis gyroscope reference, bits 2:0 = 001
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_1, &tempData );
  if( res == false ) return false;

  // Configure Gyro and Accelerometer
  // Disable FSYNC and set accelerometer and gyro bandwidth to 44 and 42 Hz, respectively;
  // DLPF_CFG = bits 2:0 = 010; this sets the sample rate at 1 kHz for both
  // Maximum delay is 4.9 ms which is just over a 200 Hz maximum rate
  tempData = 0x03;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_CONFIG, &tempData ); // Write a one to bit 7 reset bit; toggle reset device
  if( res == false ) return false;

  // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
  tempData = 0x04;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_SMPLRT_DIV, &tempData ); // Use a 200 Hz rate; the same rate set in CONFIG above
  if( res == false ) return false;

  // Set gyroscope full scale range
  // Range selects FS_SEL and AFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
  // get current GYRO_CONFIG register value
  res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_GYRO_CONFIG, 1, &tempData );
  if( res == false ) return false;

  tempData = tempData & ~0xE0; // Clear self-test bits [7:5]
  tempData = tempData & ~0x02; // Clear Fchoice bits [1:0]
  tempData = tempData & ~0x18; // Clear GFS bits [4:3]
  tempData = tempData | mpuCtrl.gyroFS << 3;  // Set full scale range for the gyro
  // Write new GYRO_CONFIG value to register
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_GYRO_CONFIG, &tempData );
  if( res == false ) return false;

  // Set accelerometer full-scale range configuration
  res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_ACCEL_CONFIG, 1, &tempData ); // Get current ACCEL_CONFIG register value
  if( res == false ) return false;

  tempData = tempData & ~0xE0; // Clear self-test bits [7:5]
  tempData = tempData & ~0x18;  // Clear AFS bits [4:3]
  tempData = tempData | mpuCtrl.accelFS << 3; // Set full scale range for the accelerometer
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_ACCEL_CONFIG, &tempData ); // Write new ACCEL_CONFIG register value
  if( res == false ) return false;

  // Set accelerometer sample rate configuration
  // It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
  // accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
  res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_ACCEL_CONFIG2, 1, &tempData ); // get current ACCEL_CONFIG2 register value
  if( res == false ) return false;

  tempData = tempData & ~0x0F; // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
  tempData = tempData | 0x03;  // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_ACCEL_CONFIG, &tempData ); // Write new ACCEL_CONFIG2 register value
  if( res == false ) return false;

  // Configure Interrupts and Bypass Enable
  // Set interrupt pin active high, push-pull, hold interrupt pin level HIGH
  // until interrupt cleared, clear on read of INT_STATUS, and enable
  // I2C_BYPASS_EN so STM can access magnetometer registers directly

  tempData = 0x22;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_INT_PIN_CFG, &tempData );
  if( res == false ) return false;

  // Enable data ready (bit 0) interrupt
  tempData = 0x01;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_INT_ENABLE, &tempData );
  if( res == false ) return false;

  return true;
}

static bool MPU9250_calibrate( float* gyroDestBias, float* accelDestBias )
{
  uint8_t data[12] = {0}; // data array to hold accelerometer and gyro x, y, z, data
  uint16_t ii, packet_count, fifo_count;
  int32_t gyro_bias[3] = {0, 0, 0};
  int32_t accel_bias[3] = {0, 0, 0};
  bool res = false;
  uint8_t tempData;

  // reset device, reset all registers, clear gyro and accelerometer bias registers
  tempData = 0x80;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_1, &tempData ); // Write a one to bit 7 reset bit; toggle reset device
  if( res == false ) return false;
  vTaskDelay( 100 );

  // get stable time source
  // Set clock source to be PLL with x-axis gyroscope reference, bits 2:0 = 001
  tempData = 0x01;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_1, &tempData );
  if( res == false ) return false;

  tempData = 0x00;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_2, &tempData );
  if( res == false ) return false;
  vTaskDelay( 200 );

  // Configure device for bias calculation
  tempData = 0x00;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_INT_ENABLE, &tempData ); // Disable all interrupts
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_FIFO_EN, &tempData ); // Disable FIFO
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_PWR_MGMT_1, &tempData ); // Turn on internal clock source
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_I2C_MST_CTRL, &tempData ); // Disable I2C master
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_USER_CTRL, &tempData ); // Disable FIFO and I2C master modes
  tempData = 0x0C;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_USER_CTRL, &tempData ); // Reset FIFO and DMP
  if( res == false ) return false;

  vTaskDelay( 15 );

  // Configure MPU9250 gyro and accelerometer for bias calculation
  tempData = 0x01;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_CONFIG, &tempData ); // Set low-pass filter to 184 Hz
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_SMPLRT_DIV, &tempData ); // Set sample rate to 1 kHz
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_GYRO_CONFIG, &tempData ); // Set gyro full-scale to 250 degrees per second, maximum sensitivity
  tempData = 0x00;
  res &= MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_ACCEL_CONFIG, &tempData ); // Set accelerometer full-scale to 2 g, maximum sensitivity
  if( res == false ) return false;

  uint16_t  gyrosensitivity  = 131;     // 16bit = 250 dps => 131 LSB/dps
  uint16_t  accelsensitivity = 16384;   // 16bit = 2g => 16384 LSB/g

  // Configure FIFO to capture accelerometer and gyro data for bias calculation
  tempData = 0x40;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_USER_CTRL, &tempData ); // Enable FIFO
  if( res == false ) return false;

  tempData = 0x78;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_FIFO_EN, &tempData ); // Enable gyro and accelerometer sensors for FIFO (max size 512 bytes in MPU-9250)
  if( res == false ) return false;
  vTaskDelay( 35 ); // this delay gives around 480 samples

  // At end of sample accumulation, turn off FIFO sensor read
  tempData = 0x00;
  res = MPU9250_writeReg( MPU9250_DEV_ADRESS, MPU9250_REG_FIFO_EN, &tempData ); // Disable gyro and accelerometer sensors for FIFO
  if( res == false ) return false;

  res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_FIFO_COUNTH, 2, (uint8_t*)&data ); // read FIFO sample count
  if( res == false ) return false;

  fifo_count = ( ( uint16_t )data[0] << 8) | data[1];
  packet_count = fifo_count / 12; //divide byte count in FIFO by size of single data packet (12 bytes)

  res = true; // initial result is true, if any subsequent read fails it is AND-ed with 0, thus false

  for (ii = 0; ii < packet_count; ii++)
  {
    int16_t accel_temp[3] = {0, 0, 0}, gyro_temp[3] = {0, 0, 0};
    res &= MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_FIFO_R_W, 12, (uint8_t*)&data ); // read data for averaging

    // Form signed 16-bit integer for each sample in FIFO
    accel_temp[0] = ( int16_t ) ((( int16_t )data[0] << 8) | data[1]  );  // Accel X
    accel_temp[1] = ( int16_t ) ((( int16_t )data[2] << 8) | data[3]  );  // Accel Y
    accel_temp[2] = ( int16_t ) ((( int16_t )data[4] << 8) | data[5]  );  // Accel Z
    gyro_temp[0]  = ( int16_t ) ((( int16_t )data[6] << 8) | data[7]  );  // Gyro X
    gyro_temp[1]  = ( int16_t ) ((( int16_t )data[8] << 8) | data[9]  );  // Gyro Y
    gyro_temp[2]  = ( int16_t ) ((( int16_t )data[10] << 8) | data[11] );  // Gyro Z

    accel_bias[0] += (int32_t) accel_temp[0]; // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
    accel_bias[1] += (int32_t) accel_temp[1];
    accel_bias[2] += (int32_t) accel_temp[2];
    gyro_bias[0]  += (int32_t) gyro_temp[0];
    gyro_bias[1]  += (int32_t) gyro_temp[1];
    gyro_bias[2]  += (int32_t) gyro_temp[2];

  }

  if( res == false ) return false;

  accel_bias[0] /= (int32_t) packet_count; // Normalize sums to get average count biases
  accel_bias[1] /= (int32_t) packet_count;
  accel_bias[2] /= (int32_t) packet_count;
  gyro_bias[0]  /= (int32_t) packet_count;
  gyro_bias[1]  /= (int32_t) packet_count;
  gyro_bias[2]  /= (int32_t) packet_count;

  if(accel_bias[2] > 0L) {accel_bias[2] -= (int32_t) accelsensitivity;}  // Remove 1g (gravity) from the z-axis accelerometer bias calculation
  else {accel_bias[2] += (int32_t) accelsensitivity;}

#if 0 //TODO: Check if it improves performance
  // Calculation of gyro bias for pushing to hardware registers

  // Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
  data[0] = (-gyro_bias[0]/4  >> 8) & 0xFF; // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
  data[1] = (-gyro_bias[0]/4)       & 0xFF; // Biases are additive, so change sign on calculated average gyro biases
  data[2] = (-gyro_bias[1]/4  >> 8) & 0xFF;
  data[3] = (-gyro_bias[1]/4)       & 0xFF;
  data[4] = (-gyro_bias[2]/4  >> 8) & 0xFF;
  data[5] = (-gyro_bias[2]/4)       & 0xFF;

  /// Push gyro biases to hardware registers
  I2CResult = MPU9250_writeReg( MPU9250_REG_XG_OFFSET_H, data[0] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_XG_OFFSET_L, data[1] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_YG_OFFSET_H, data[2] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_YG_OFFSET_L, data[3] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_ZG_OFFSET_H, data[4] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_ZG_OFFSET_L, data[5] );
#endif

  gyroDestBias[0] = (float) gyro_bias[0]/(float) gyrosensitivity; // construct gyro bias in deg/s for later manual subtraction
  gyroDestBias[1] = (float) gyro_bias[1]/(float) gyrosensitivity;
  gyroDestBias[2] = (float) gyro_bias[2]/(float) gyrosensitivity;

#if 0  //TODO: Check if it improves performance
  // Calculation of accel bias for pushing to hardware registers

  // Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
  // factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
  // non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
  // compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
  // the accelerometer biases calculated above must be divided by 8.

  int32_t accel_bias_reg[3] = {0, 0, 0}; // A place to hold the factory accelerometer trim biases

  I2CResult = MPU9250_readData( MPU9250_REG_XA_OFFSET_H, 2, &data );
  accel_bias_reg[0] = (int16_t) ((int16_t)data[0] << 8) | data[1];
  I2CResult = MPU9250_readData( MPU9250_REG_YA_OFFSET_H, 2, &data );
  accel_bias_reg[1] = (int16_t) ((int16_t)data[0] << 8) | data[1];
  I2CResult = MPU9250_readData( MPU9250_REG_ZA_OFFSET_H, 2, &data );
  accel_bias_reg[2] = (int16_t) ((int16_t)data[0] << 8) | data[1];

  uint32_t mask = 1uL; // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
  uint8_t mask_bit[3] = {0, 0, 0}; // Define array to hold mask bit for each accelerometer bias axis

  for(ii = 0; ii < 3; ii++) {
    if(accel_bias_reg[ii] & mask) mask_bit[ii] = 0x01; // If temperature compensation bit is set, record that fact in mask_bit
  }

  // Construct total accelerometer bias, including calculated average accelerometer bias from above
  accel_bias_reg[0] -= (accel_bias[0]/8); // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
  accel_bias_reg[1] -= (accel_bias[1]/8);
  accel_bias_reg[2] -= (accel_bias[2]/8);

  data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
  data[1] = (accel_bias_reg[0])      & 0xFF;
  data[1] = data[1] | mask_bit[0]; // preserve temperature compensation bit when writing back to accelerometer bias registers
  data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
  data[3] = (accel_bias_reg[1])      & 0xFF;
  data[3] = data[3] | mask_bit[1]; // preserve temperature compensation bit when writing back to accelerometer bias registers
  data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
  data[5] = (accel_bias_reg[2])      & 0xFF;
  data[5] = data[5] | mask_bit[2]; // preserve temperature compensation bit when writing back to accelerometer bias registers

  // Apparently this is not working for the acceleration biases in the MPU-9250
  // Are we handling the temperature correction bit properly?
  // Push accelerometer biases to hardware registers
  I2CResult = MPU9250_writeReg( MPU9250_REG_XA_OFFSET_H, data[0] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_XA_OFFSET_L, data[1] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_YA_OFFSET_H, data[2] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_YA_OFFSET_L, data[3] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_ZA_OFFSET_H, data[4] );
  I2CResult = MPU9250_writeReg( MPU9250_REG_ZA_OFFSET_L, data[5] );
#endif

  // Output scaled accelerometer biases for manual subtraction in the main program
  accelDestBias[0] = (float)accel_bias[0]/(float)accelsensitivity;
  accelDestBias[1] = (float)accel_bias[1]/(float)accelsensitivity;
  accelDestBias[2] = (float)accel_bias[2]/(float)accelsensitivity;

  return true;
}

static bool MPU9250_initAK8963( float * magCalib )
{
  // First extract the factory calibration for each magnetometer axis
  bool res = false;
  uint8_t tempData = 0;
  uint8_t rawData[3] = {0};  // x/y/z gyro calibration data stored here

  tempData = 0x00;
  res = MPU9250_writeReg( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_CNTL, &tempData ); // Power down magnetometer
  if( res == false ) return false;
  vTaskDelay( 10 );

  tempData = 0x0F;
  res = MPU9250_writeReg( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_CNTL, &tempData ); // Enter Fuse ROM access mode
  if( res == false ) return false;
  vTaskDelay( 10 );

  res = MPU9250_readData( MPU9250_MAG_DEV_ADDRESS , MPU9250_REGM_ASAX, 3, ( uint8_t* )&rawData ); // Read the x-, y-, and z-axis calibration values
  if( res == false ) return false;
  magCalib[0] =  ( float )( rawData[0] - 128 )/256.0f + 1.0f;  // Return x-axis sensitivity adjustment values, etc.
  magCalib[1] =  ( float )( rawData[1] - 128 )/256.0f + 1.0f;  // Adjustment values calculated with formula from manual.
  magCalib[2] =  ( float )( rawData[2] - 128 )/256.0f + 1.0f;

  tempData = 0x00;
  res = MPU9250_writeReg( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_CNTL, &tempData ); // Power down magnetometer
  if( res == false ) return false;
  vTaskDelay( 10 );

  // Configure the magnetometer for continuous read and highest resolution
  // set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
  // and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates
  tempData = ( mpuCtrl.magFS << 4 ) | mpuCtrl.magODR;
  res = MPU9250_writeReg( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_CNTL, &tempData ); // Set magnetometer data resolution and sample ODR
  if( res == false ) return false;
  vTaskDelay( 10 );

  return true;
}

static void MPU9250_getSensorResolutions( void )
{
  // Accel resolution
  // Possible accelerometer scales (and their register bit settings) are:
  // 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
  switch( mpuCtrl.accelFS )
  {
    case MPU9250_AFS_2G:
      mpuCtrl.accelRes = 2.0/32768.0;
      break;
    case MPU9250_AFS_4G:
      mpuCtrl.accelRes = 4.0/32768.0;
      break;
    case MPU9250_AFS_8G:
      mpuCtrl.accelRes = 8.0/32768.0;
      break;
    case MPU9250_AFS_16G:
      mpuCtrl.accelRes = 16.0/32768.0;
      break;
  }

  // Gyro resolution
  // Possible gyro scales (and their register bit settings) are:
  // 250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS  (11).
  switch( mpuCtrl.gyroFS )
  {
    case MPU9250_GFS_250DPS:
      mpuCtrl.gyroRes = 250.0/32768.0;
      break;
    case MPU9250_GFS_500DPS:
      mpuCtrl.gyroRes = 500.0/32768.0;
      break;
    case MPU9250_GFS_1000DPS:
      mpuCtrl.gyroRes = 1000.0/32768.0;
      break;
    case MPU9250_GFS_2000DPS:
      mpuCtrl.gyroRes = 2000.0/32768.0;
      break;
  }

  // Mag resolution
  // Possible magnetometer scales (and their register bit settings) are:
  // 14 bit resolution (0) and 16 bit resolution (1)
  switch( mpuCtrl.magFS )
  {
    case MPU9250_MFS_14BITS:
      mpuCtrl.magRes = 10.0*4912.0/8190.0; // Proper scale to return milliGauss
      break;
    case MPU9250_MFS_16BITS:
      mpuCtrl.magRes = 10.0*4912.0/32760.0; // Proper scale to return milliGauss
      break;
  }
}

static bool MPU9250_readAccel( int16_t* data )
{
  uint8_t rawData[6] = {0};
  bool res = false;

  res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_ACCEL_XOUT_H, 6, ( uint8_t* )&rawData );  // Read sensor registers - 2 for each of 3 axis
  if( res == false ) return false;

  data[0] = (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]) ; // Create 16-bit readings from registers
  data[1] = (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]) ;
  data[2] = (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]) ;

  return true;
}

static bool MPU9250_readGyro( int16_t* data )
{
  uint8_t rawData[6] = {0};
  bool res = false;

  res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_GYRO_XOUT_H, 6, ( uint8_t* )&rawData );  // Read sensor registers - 2 for each of 3 axis
  if( res == false ) return false;

  data[0] = (int16_t)(((int16_t)rawData[0] << 8) | rawData[1]) ; // Create 16-bit readings from registers
  data[1] = (int16_t)(((int16_t)rawData[2] << 8) | rawData[3]) ;
  data[2] = (int16_t)(((int16_t)rawData[4] << 8) | rawData[5]) ;

  return true;
}

static bool MPU9250_readMag( int16_t* data )
{
  uint8_t rawData[7] = {0};  // 7 bytes because ST2 needs to be read after 6 mag data registers
  uint8_t temp = 0;
  bool res = false;
  bool returnValue = false;

  res = MPU9250_readData( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_ST1, 1, ( uint8_t* )&temp);

  if( res == true && ( temp & 0x01 ) ) // check if read correct and data ready for reading
  {
    res = MPU9250_readData( MPU9250_MAG_DEV_ADDRESS, MPU9250_REGM_XOUT_L, 7, ( uint8_t* )&rawData);

    if( res == true && ( !(rawData[6] & 0x08) ) )  // check if read correct and no data overflow
    {
      data[0] = ( int16_t )( ( ( int16_t )rawData[1] << 8 ) | rawData[0] );  // Create 16-bit readings from registers
      data[1] = ( int16_t )( ( ( int16_t )rawData[3] << 8 ) | rawData[2] );  // Data stored as little Endian
      data[2] = ( int16_t )( ( ( int16_t )rawData[5] << 8 ) | rawData[4] );
      returnValue = true;
    }
  }

  return returnValue;
}

static bool MPU9250_writeReg( uint8_t devAddr, uint8_t regAddr, uint8_t *data )
{

  HAL_StatusTypeDef I2CResult = HAL_ERROR;
  bool result;

  I2C1_LOCK();  // obtain lock on I2C1 port

  I2C_writeData( I2C_PORT_1, devAddr, regAddr, 1, data, mpuCtrl.taskHndl, &I2CResult );  //request I2C write

  ulTaskNotifyTake(pdTRUE, portMAX_DELAY ); // wait for notification from I2C task indicating request done
  I2C1_UNLOCK();  // unlock I2C1 port

  result = ( ( I2CResult == HAL_OK ) ? true : false );

  return result;

}

static bool MPU9250_readData( uint8_t devAddr, uint8_t regAddr, uint8_t size, uint8_t *data )
{
  HAL_StatusTypeDef I2CResult = HAL_ERROR;
  bool result;

  I2C1_LOCK();  // obtain lock on I2C1 port

  I2C_readData( I2C_PORT_1, devAddr, regAddr, size, data, mpuCtrl.taskHndl, &I2CResult );  //request I2C read

  ulTaskNotifyTake(pdTRUE, portMAX_DELAY ); // wait for notification from I2C task indicating request done
  I2C1_UNLOCK();  // unlock I2C1 port

  result = ( ( I2CResult == HAL_OK ) ? true : false );

  return result;

}

static void MPU9250_convertReadings( void )
{
  // Convert accelerometer readings into g's
  localOutData.accel[0] = (float)readings.accel[0] * mpuCtrl.accelRes - mpuCtrl.accelBias[0];
  localOutData.accel[1] = (float)readings.accel[1] * mpuCtrl.accelRes - mpuCtrl.accelBias[1];
  localOutData.accel[2] = (float)readings.accel[2] * mpuCtrl.accelRes - mpuCtrl.accelBias[2];

  // Convert gyroscope values into degrees per second
  localOutData.gyro[0] = (float)readings.gyro[0] * mpuCtrl.gyroRes - mpuCtrl.gyroBias[0];
  localOutData.gyro[1] = (float)readings.gyro[1] * mpuCtrl.gyroRes - mpuCtrl.gyroBias[1];
  localOutData.gyro[2] = (float)readings.gyro[2] * mpuCtrl.gyroRes - mpuCtrl.gyroBias[2];

  // Convert magnetometer values in milliGauss
  // Include factory calibration per data sheet and user environmental corrections
  localOutData.mag[0] = (float)readings.mag[0] * mpuCtrl.magRes * mpuCtrl.magCalib[0] - mpuCtrl.magBias[0];
  localOutData.mag[1] = (float)readings.mag[1] * mpuCtrl.magRes * mpuCtrl.magCalib[1] - mpuCtrl.magBias[1];
  localOutData.mag[2] = (float)readings.mag[2] * mpuCtrl.magRes * mpuCtrl.magCalib[2] - mpuCtrl.magBias[2];
  localOutData.mag[0] *= mpuCtrl.magScale[0];
  localOutData.mag[1] *= mpuCtrl.magScale[1];
  localOutData.mag[2] *= mpuCtrl.magScale[2];
}

static float MPU9250_calcIterationTime( uint32_t prevReadTime, uint32_t newReadTime )
{
  float iterTime = 0;
  if( newReadTime >= prevReadTime )
  {
    iterTime = ( ( float )( newReadTime - prevReadTime ) ) / 1000.0 ;
  }
  else
  {
    iterTime = ( ( float )( ( UINT32_MAX - prevReadTime ) + newReadTime ) ) / 1000.0 ;
  }

  return iterTime;
}
#ifdef USE_X_IO_MADGWICK
static void MPU9250_madgwickQuaternionUpdate( float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz )
{

  float q1 = calcData.q[0], q2 = calcData.q[1], q3 = calcData.q[2], q4 = calcData.q[3];   // short name local variable for readability
  float norm;
  float hx, hy, _2bx, _2bz;
  float s1, s2, s3, s4;
  float qDot1, qDot2, qDot3, qDot4;

  // Auxiliary variables to avoid repeated arithmetic
  float _2q1mx;
  float _2q1my;
  float _2q1mz;
  float _2q2mx;
  float _4bx;
  float _4bz;
  float _2q1 = 2.0f * q1;
  float _2q2 = 2.0f * q2;
  float _2q3 = 2.0f * q3;
  float _2q4 = 2.0f * q4;
  float _2q1q3 = 2.0f * q1 * q3;
  float _2q3q4 = 2.0f * q3 * q4;
  float q1q1 = q1 * q1;
  float q1q2 = q1 * q2;
  float q1q3 = q1 * q3;
  float q1q4 = q1 * q4;
  float q2q2 = q2 * q2;
  float q2q3 = q2 * q3;
  float q2q4 = q2 * q4;
  float q3q3 = q3 * q3;
  float q3q4 = q3 * q4;
  float q4q4 = q4 * q4;

  // Normalise accelerometer measurement
  norm = sqrt(ax * ax + ay * ay + az * az);
  if (norm == 0.0f) return; // handle NaN
  norm = 1.0f/norm;
  ax *= norm;
  ay *= norm;
  az *= norm;

  // Normalise magnetometer measurement
  norm = sqrt(mx * mx + my * my + mz * mz);
  if (norm == 0.0f) return; // handle NaN
  norm = 1.0f/norm;
  mx *= norm;
  my *= norm;
  mz *= norm;

  // Reference direction of Earth's magnetic field
  _2q1mx = 2.0f * q1 * mx;
  _2q1my = 2.0f * q1 * my;
  _2q1mz = 2.0f * q1 * mz;
  _2q2mx = 2.0f * q2 * mx;
  hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4;
  hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4;
  _2bx = sqrt(hx * hx + hy * hy);
  _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4;
  _4bx = 2.0f * _2bx;
  _4bz = 2.0f * _2bz;

  // Gradient decent algorithm corrective step
  s1 = -_2q3 * (2.0f * q2q4 - _2q1q3 - ax) + _2q2 * (2.0f * q1q2 + _2q3q4 - ay) - _2bz * q3 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q4 + _2bz * q2) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
  s2 = _2q4 * (2.0f * q2q4 - _2q1q3 - ax) + _2q1 * (2.0f * q1q2 + _2q3q4 - ay) - 4.0f * q2 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az) + _2bz * q4 * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q3 + _2bz * q1) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q4 - _4bz * q2) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
  s3 = -_2q1 * (2.0f * q2q4 - _2q1q3 - ax) + _2q4 * (2.0f * q1q2 + _2q3q4 - ay) - 4.0f * q3 * (1.0f - 2.0f * q2q2 - 2.0f * q3q3 - az) + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
  s4 = _2q2 * (2.0f * q2q4 - _2q1q3 - ax) + _2q3 * (2.0f * q1q2 + _2q3q4 - ay) + (-_4bx * q4 + _2bz * q2) * (_2bx * (0.5f - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q1 + _2bz * q3) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my) + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5f - q2q2 - q3q3) - mz);
  norm = sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4);    // normalise step magnitude
  norm = 1.0f/norm;
  s1 *= norm;
  s2 *= norm;
  s3 *= norm;
  s4 *= norm;

  // Compute rate of change of quaternion
  qDot1 = 0.5f * (-q2 * gx - q3 * gy - q4 * gz) - calcData.beta * s1;
  qDot2 = 0.5f * (q1 * gx + q3 * gz - q4 * gy) - calcData.beta * s2;
  qDot3 = 0.5f * (q1 * gy - q2 * gz + q4 * gx) - calcData.beta * s3;
  qDot4 = 0.5f * (q1 * gz + q2 * gy - q3 * gx) - calcData.beta * s4;

  // Integrate to yield quaternion
  q1 += qDot1 * mpuCtrl.iterationTime;
  q2 += qDot2 * mpuCtrl.iterationTime;
  q3 += qDot3 * mpuCtrl.iterationTime;
  q4 += qDot4 * mpuCtrl.iterationTime;
  norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);    // normalise quaternion
  norm = 1.0f/norm;
  calcData.q[0] = q1 * norm;
  calcData.q[1] = q2 * norm;
  calcData.q[2] = q3 * norm;
  calcData.q[3] = q4 * norm;
}




#else

static void MPU9250_madgwickQuaternionUpdate( float a_x, float a_y, float a_z, float w_x, float w_y, float w_z, float m_x, float m_y, float m_z )
{
  float SEq_1 = calcData.q[0], SEq_2 = calcData.q[1], SEq_3 = calcData.q[2], SEq_4 = calcData.q[3]; // estimated orientation quaternion elements with initial conditions
  // local system variables
  float norm; // vector norm
  float SEqDot_omega_1, SEqDot_omega_2, SEqDot_omega_3, SEqDot_omega_4; // quaternion rate from gyroscopes elements
  float f_1, f_2, f_3, f_4, f_5, f_6;                                   // objective function elements
  float J_11or24, J_12or23, J_13or22, J_14or21, J_32, J_33;             // objective function Jacobian elements
  float J_41, J_42, J_43, J_44, J_51, J_52, J_53, J_54, J_61, J_62, J_63, J_64; // objective function Jacobian elements
  float SEqHatDot_1, SEqHatDot_2, SEqHatDot_3, SEqHatDot_4; // estimated direction of the gyroscope error
  float w_err_x, w_err_y, w_err_z;                          // estimated direction of the gyroscope error (angular)
  float h_x, h_y, h_z;                                      // computed flux in the earth frame

  // axulirary variables to avoid reapeated calcualtions
  float halfSEq_1 = 0.5f * SEq_1;
  float halfSEq_2 = 0.5f * SEq_2;
  float halfSEq_3 = 0.5f * SEq_3;
  float halfSEq_4 = 0.5f * SEq_4;
  float twoSEq_1 = 2.0f * SEq_1;
  float twoSEq_2 = 2.0f * SEq_2;
  float twoSEq_3 = 2.0f * SEq_3;
  float twoSEq_4 = 2.0f * SEq_4;
  float twob_x = 2.0f * calcData.flux[0];
  float twob_z = 2.0f * calcData.flux[1];
  float twob_xSEq_1 = 2.0f * calcData.flux[0] * SEq_1;
  float twob_xSEq_2 = 2.0f * calcData.flux[0] * SEq_2;
  float twob_xSEq_3 = 2.0f * calcData.flux[0] * SEq_3;
  float twob_xSEq_4 = 2.0f * calcData.flux[0] * SEq_4;
  float twob_zSEq_1 = 2.0f * calcData.flux[1] * SEq_1;
  float twob_zSEq_2 = 2.0f * calcData.flux[1] * SEq_2;
  float twob_zSEq_3 = 2.0f * calcData.flux[1] * SEq_3;
  float twob_zSEq_4 = 2.0f * calcData.flux[1] * SEq_4;
  float SEq_1SEq_2;
  float SEq_1SEq_3 = SEq_1 * SEq_3;
  float SEq_1SEq_4;
  float SEq_2SEq_3;
  float SEq_2SEq_4 = SEq_2 * SEq_4;
  float SEq_3SEq_4;
  float twom_x = 2.0f * m_x;
  float twom_y = 2.0f * m_y;
  float twom_z = 2.0f * m_z;
  // normalise the accelerometer measurement
  norm = sqrt(a_x * a_x + a_y * a_y + a_z * a_z);
  a_x /= norm;
  a_y /= norm;
  a_z /= norm;
  // normalise the magnetometer measurement
  norm = sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
  m_x /= norm;
  m_y /= norm;
  m_z /= norm;
  // compute the objective function and Jacobian
  f_1 = twoSEq_2 * SEq_4 - twoSEq_1 * SEq_3 - a_x;
  f_2 = twoSEq_1 * SEq_2 + twoSEq_3 * SEq_4 - a_y;
  f_3 = 1.0f - twoSEq_2 * SEq_2 - twoSEq_3 * SEq_3 - a_z;
  f_4 = twob_x * (0.5f - SEq_3 * SEq_3 - SEq_4 * SEq_4) + twob_z * (SEq_2SEq_4 - SEq_1SEq_3) - m_x;
  f_5 = twob_x * (SEq_2 * SEq_3 - SEq_1 * SEq_4) + twob_z * (SEq_1 * SEq_2 + SEq_3 * SEq_4) - m_y;
  f_6 = twob_x * (SEq_1SEq_3 + SEq_2SEq_4) + twob_z * (0.5f - SEq_2 * SEq_2 - SEq_3 * SEq_3) - m_z;
  J_11or24 = twoSEq_3; // J_11 negated in matrix multiplication
  J_12or23 = 2.0f * SEq_4;
  J_13or22 = twoSEq_1; // J_12 negated in matrix multiplication
  J_14or21 = twoSEq_2;
  J_32 = 2.0f * J_14or21; // negated in matrix multiplication
  J_33 = 2.0f * J_11or24; // negated in matrix multiplication
  J_41 = twob_zSEq_3; // negated in matrix multiplication
  J_42 = twob_zSEq_4;
  J_43 = 2.0f * twob_xSEq_3 + twob_zSEq_1; // negated in matrix multiplication
  J_44 = 2.0f * twob_xSEq_4 - twob_zSEq_2; // negated in matrix multiplication
  J_51 = twob_xSEq_4 - twob_zSEq_2; // negated in matrix multiplication
  J_52 = twob_xSEq_3 + twob_zSEq_1;
  J_53 = twob_xSEq_2 + twob_zSEq_4;
  J_54 = twob_xSEq_1 - twob_zSEq_3; // negated in matrix multiplication
  J_61 = twob_xSEq_3;
  J_62 = twob_xSEq_4 - 2.0f * twob_zSEq_2;
  J_63 = twob_xSEq_1 - 2.0f * twob_zSEq_3;
  J_64 = twob_xSEq_2;

  // compute the gradient (matrix multiplication)
  SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1 - J_41 * f_4 - J_51 * f_5 + J_61 * f_6;
  SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3 + J_42 * f_4 + J_52 * f_5 + J_62 * f_6;
  SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1 - J_43 * f_4 + J_53 * f_5 + J_63 * f_6;
  SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2 - J_44 * f_4 - J_54 * f_5 + J_64 * f_6;
  // normalise the gradient to estimate direction of the gyroscope error
  norm = sqrt(SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4);
  SEqHatDot_1 = SEqHatDot_1 / norm;
  SEqHatDot_2 = SEqHatDot_2 / norm;

  SEqHatDot_3 = SEqHatDot_3 / norm;
  SEqHatDot_4 = SEqHatDot_4 / norm;
  // compute angular estimated direction of the gyroscope error
  w_err_x = twoSEq_1 * SEqHatDot_2 - twoSEq_2 * SEqHatDot_1 - twoSEq_3 * SEqHatDot_4 + twoSEq_4 * SEqHatDot_3;
  w_err_y = twoSEq_1 * SEqHatDot_3 + twoSEq_2 * SEqHatDot_4 - twoSEq_3 * SEqHatDot_1 - twoSEq_4 * SEqHatDot_2;
  w_err_z = twoSEq_1 * SEqHatDot_4 - twoSEq_2 * SEqHatDot_3 + twoSEq_3 * SEqHatDot_2 - twoSEq_4 * SEqHatDot_1;
  // compute and remove the gyroscope biases
  calcData.gyroBiasErr[0] += w_err_x * mpuCtrl.iterationTime * calcData.zeta;
  calcData.gyroBiasErr[1] += w_err_y * mpuCtrl.iterationTime * calcData.zeta;
  calcData.gyroBiasErr[2] += w_err_z * mpuCtrl.iterationTime * calcData.zeta;
  w_x -= calcData.gyroBiasErr[0];
  w_y -= calcData.gyroBiasErr[1];
  w_z -= calcData.gyroBiasErr[2];
  // compute the quaternion rate measured by gyroscopes
  SEqDot_omega_1 = -halfSEq_2 * w_x - halfSEq_3 * w_y - halfSEq_4 * w_z;
  SEqDot_omega_2 = halfSEq_1 * w_x + halfSEq_3 * w_z - halfSEq_4 * w_y;
  SEqDot_omega_3 = halfSEq_1 * w_y - halfSEq_2 * w_z + halfSEq_4 * w_x;
  SEqDot_omega_4 = halfSEq_1 * w_z + halfSEq_2 * w_y - halfSEq_3 * w_x;
  // compute then integrate the estimated quaternion rate
  SEq_1 += (SEqDot_omega_1 - (calcData.beta * SEqHatDot_1)) * mpuCtrl.iterationTime;
  SEq_2 += (SEqDot_omega_2 - (calcData.beta * SEqHatDot_2)) * mpuCtrl.iterationTime;
  SEq_3 += (SEqDot_omega_3 - (calcData.beta * SEqHatDot_3)) * mpuCtrl.iterationTime;
  SEq_4 += (SEqDot_omega_4 - (calcData.beta * SEqHatDot_4)) * mpuCtrl.iterationTime;
  // normalise quaternion
  norm = sqrt(SEq_1 * SEq_1 + SEq_2 * SEq_2 + SEq_3 * SEq_3 + SEq_4 * SEq_4);
  SEq_1 /= norm;
  SEq_2 /= norm;
  SEq_3 /= norm;
  SEq_4 /= norm;
  // compute flux in the earth frame
  SEq_1SEq_2 = SEq_1 * SEq_2; // recompute axulirary variables
  SEq_1SEq_3 = SEq_1 * SEq_3;
  SEq_1SEq_4 = SEq_1 * SEq_4;
  SEq_3SEq_4 = SEq_3 * SEq_4;
  SEq_2SEq_3 = SEq_2 * SEq_3;
  SEq_2SEq_4 = SEq_2 * SEq_4;
  h_x = twom_x * (0.5f - SEq_3 * SEq_3 - SEq_4 * SEq_4) + twom_y * (SEq_2SEq_3 - SEq_1SEq_4) + twom_z * (SEq_2SEq_4 + SEq_1SEq_3);
  h_y = twom_x * (SEq_2SEq_3 + SEq_1SEq_4) + twom_y * (0.5f - SEq_2 * SEq_2 - SEq_4 * SEq_4) + twom_z * (SEq_3SEq_4 - SEq_1SEq_2);
  h_z = twom_x * (SEq_2SEq_4 - SEq_1SEq_3) + twom_y * (SEq_3SEq_4 + SEq_1SEq_2) + twom_z * (0.5f - SEq_2 * SEq_2 - SEq_3 * SEq_3);
  // normalise the flux vector to have only components in the x and z
  calcData.flux[0] = sqrt((h_x * h_x) + (h_y * h_y));
  calcData.flux[1] = h_z;

  calcData.q[0] = SEq_1;
  calcData.q[1] = SEq_2;
  calcData.q[2] = SEq_3;
  calcData.q[3] = SEq_4;
}
#endif


static void MPU9250_calcAngles( float* q )
{
  float yaw, pitch, roll;
#ifdef USE_X_IO_MADGWICK
  yaw   = atan2(2.0f * (q[1] * q[2] + q[0] * q[3]), q[0] * q[0] + q[1] * q[1] - q[2] * q[2] - q[3] * q[3]);
  pitch = -asin(2.0f * (q[1] * q[3] - q[0] * q[2]));
  roll  = atan2(2.0f * (q[0] * q[1] + q[2] * q[3]), q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3]);
#else
  yaw   = atan2(2.0f * (q[1] * q[2] - q[0] * q[3]), 2.0f * q[0] * q[0] + 2.0f * q[1] * q[1] - 1 );
  pitch = -asin(2.0f * (q[1] * q[3] + q[0] * q[2]));
  roll  = atan2(2.0f * (q[2] * q[3] - q[0] * q[1]), 2.0f * q[0] * q[0] + 2.0f * q[3] * q[3] - 1);
#endif
  pitch *= 180.0f / PI;
  yaw   *= 180.0f / PI;
  yaw   += calcData.decl;
  roll  *= 180.0f / PI;

  localOutData.yaw = yaw;
  localOutData.pitch = pitch;
  localOutData.roll = roll;
}

static void MPU9250_updatePublicData( void )
{
  MPU9250_DATA_LOCK();
  memcpy( &publicOutData, &localOutData, sizeof( T_MPU9250_OUT_DATA ) );
  MPU9250_DATA_UNLOCK();
}

static void MPU9250_invalidateOutData( void )
{
  localOutData.valid = false;
  MPU9250_DATA_LOCK();
  publicOutData.valid = false;
  MPU9250_DATA_UNLOCK();
}

static void MPU9250_setState( T_MPU9250_ST state )
{
  if( mpuCtrl.state != state )
  {
    //DBGOUT("MPU9250: st %u -> %u\n", mpuCtrl.state, state );
    mpuCtrl.state = state;
  }
}

static void MPU9250_setSetupState( T_MPU9250_SETUP_ST state )
{
  if( mpuCtrl.setupState != state )
  {
    //DBGOUT("MPU9250: stpSt %u -> %u\n", mpuCtrl.setupState, state );
    mpuCtrl.setupState = state;
  }
}


//========EXPORTED FUNCTIONS DEFINITIONS========

void vTaskMPU9250( void *p )
{
  while(1)
  {
    bool res = false;
    uint8_t tempData = 0;

    switch( mpuCtrl.state )
    {
      case MPU9250_ST_NONE:
        MPU9250_setSetupState( MPU9250_SETUP_ST_START );
        MPU9250_setState( MPU9250_ST_SETUP );
        break;

      case MPU9250_ST_SETUP:

        if( mpuCtrl.deactivateRequest )
        {
          vTaskSuspend( NULL );   //tasks suspends itself if requested
        }

        MPU9250_setup();

        if( mpuCtrl.setupState == MPU9250_SETUP_ST_ERROR )
        {
          DBGOUT( "MPU9250: Setup failed!\n" );
          MPU9250_setState( MPU9250_ST_ERROR );
        }
        else if( mpuCtrl.setupState == MPU9250_SETUP_ST_DONE )
        {
          MPU9250_setState( MPU9250_ST_READING_DATA );
        }
        break;

      case MP9250_ST_WAITING_DATA_RDY:
        //TODO: timeout
        if( mpuCtrl.deactivateRequest )
        {
          vTaskSuspend( NULL );   //tasks suspends itself if requested
        }

        res = MPU9250_readData( MPU9250_DEV_ADRESS, MPU9250_REG_INT_STATUS, 1, &tempData ); //TODO: error state if !res

        if( res == true && ( tempData & 0x01 ) )  // if read correct and data ready
        {
          MPU9250_setState( MPU9250_ST_READING_DATA );
        }
        break;

      case MPU9250_ST_READING_DATA:

        res = MPU9250_readAccel( ( int16_t* )&readings.accel );
        res &= MPU9250_readGyro( ( int16_t* )&readings.gyro );
        res &= MPU9250_readMag( ( int16_t* )&readings.mag );

        if( res == true )
        {
          MPU9250_setState( MPU9250_ST_CALCULATING );
          readings.timeStamp = xTaskGetTickCount();
        }

        else
        {
          MPU9250_setState( MPU9250_ST_ERROR );
        }

        break;

      case MPU9250_ST_CALCULATING:
        MPU9250_convertReadings();
        mpuCtrl.iterationTime = MPU9250_calcIterationTime( mpuCtrl.lastReadTimeStamp, readings.timeStamp );

        mpuCtrl.lastReadTimeStamp = readings.timeStamp;
#if 0
        DBGOUT( "Ax: %02f, Ay: %02f, Az: %02f\n", localOutData.accel[0], localOutData.accel[1], localOutData.accel[2] );
        DBGOUT( "Gx: %02f, Gy: %02f, Gz: %02f\n", localOutData.gyro[0], localOutData.gyro[1], localOutData.gyro[2] );
        DBGOUT( "Mx: %02f, My: %02f, Mz: %02f\n\n", localOutData.mag[0], localOutData.mag[1], localOutData.mag[2] );

        DBGOUT( "%02f %02f %02f ", localOutData.accel[0], localOutData.accel[1], localOutData.accel[2] );
        DBGOUT( "%02f %02f %02f ", localOutData.gyro[0], localOutData.gyro[1], localOutData.gyro[2] );
        DBGOUT( "%02f %02f %02f\n", localOutData.mag[0], localOutData.mag[1], localOutData.mag[2] );
#endif

        MPU9250_madgwickQuaternionUpdate( -localOutData.accel[1], -localOutData.accel[0], localOutData.accel[2],
                                          localOutData.gyro[1]*PI/180.0f, localOutData.gyro[0]*PI/180.0f, -localOutData.gyro[2]*PI/180.0f,
                                          localOutData.mag[0], localOutData.mag[1], localOutData.mag[2] );  //TODO: refactor

        MPU9250_calcAngles( ( float* )&calcData.q );

        //DBGOUT( "%02f , %02f ,  %02f\n", localOutData.yaw, localOutData.pitch, localOutData.roll );

        localOutData.valid = true;  // If execution reached this point, mark data as valid

        MPU9250_setState( MPU9250_ST_UPDATING_DATA );

        break;

      case MPU9250_ST_UPDATING_DATA:
        MPU9250_updatePublicData();
        MPU9250_setState( MP9250_ST_WAITING_DATA_RDY );
        break;

      case MPU9250_ST_ERROR:
        MPU9250_invalidateOutData();

        MPU9250_setState( MPU9250_ST_NONE );  // Go to 'none' state to reinitialize device
        break;

      default:
        DBGOUT( "MPU9250: Unexpected state\n" );
        MPU9250_setState( MPU9250_ST_ERROR );
        break;
    }
  }
}

// Initialize variables and states
void MPU9250_initialize( void )
{
  mpuCtrl.accelFS = MPU9250_AFS_2G;
  mpuCtrl.gyroFS = MPU9250_GFS_250DPS;
  mpuCtrl.magFS = MPU9250_MFS_16BITS;
  mpuCtrl.magODR = 0x06;        // 8 Hz (0x02) or 100 Hz (0x06) magnetometer ODR
  mpuCtrl.state = MPU9250_ST_NONE;
  mpuCtrl.setupState = MPU9250_SETUP_ST_NONE;
  mpuCtrl.deactivateRequest = false;

  //TODO: consider magbias calculation in calibrating procedure, instead of hardcoding
  mpuCtrl.magBias[0] = 25.987;   // User environmental x-axis correction in milliGauss
  mpuCtrl.magBias[1]= 493.762;   // User environmental y-axis correction in milliGauss
  mpuCtrl.magBias[2] = -351.61;  // User environmental z-axis correction in milliGauss
  mpuCtrl.magScale[0] = 0.9659;
  mpuCtrl.magScale[1] = 1.0261;
  mpuCtrl.magScale[2] = 1.0099;

  calcData.q[0] = 1.0f;
  calcData.q[1] = 0.0f;
  calcData.q[2] = 0.0f;
  calcData.q[3] = 0.0f;
#ifndef USE_X_IO_MADGWICK
  calcData.beta = sqrt(3.0f / 4.0f) * (PI * (0.0f / 180.0f));
  calcData.zeta = sqrt(3.0f / 4.0f) * (PI * (0.0f / 180.0f));
  calcData.flux[0] = 1;
  calcData.flux[1] = 0;
  calcData.gyroBiasErr[0] = 0;
  calcData.gyroBiasErr[1] = 0;
  calcData.gyroBiasErr[2] = 0;
#else
  calcData.beta = sqrt(3.0f / 4.0f) * (PI * (60.0f / 180.0f));
#endif
  calcData.decl = 5.46f;    //TODO: verify

  if( mpuCtrl.taskHndl )  // MPU task resumed after being suspended
  {
    vTaskResume( mpuCtrl.taskHndl );
  }
  else  // First initialisation - create task and mutex
  {
    MPU9250_initMP9250Task();
    MPU9250DataMutex = xSemaphoreCreateMutex();
    ASSERT( MPU9250DataMutex );
  }

  MPU9250_invalidateOutData();  // Consider data invalid until first successful read.
}

void MPU9250_deactivate( void )
{
  mpuCtrl.deactivateRequest = true;
}

void MPU9250_setBeta( float beta )
{
  DBGOUT("MPU9250: Set Beta %.05f\n", beta);
  calcData.beta = beta;
}
#ifndef USE_X_IO_MADGWICK
void MPU9250_setZeta( float zeta )
{
  DBGOUT("MPU9250: Set Zeta %.05f\n", zeta);
  calcData.zeta = zeta;
}
#endif

void MPU9250_setQuaternions(void)
{
  calcData.q[0] = 5;
  calcData.q[1] = 0.7;
  calcData.q[2] = -0.1;
  calcData.q[3] = 2.4;
}

T_MPU9250_OUT_DATA* MPU9250_getPublicDataPtr( void )
{
  return ( T_MPU9250_OUT_DATA* )&publicOutData;
}

TaskHandle_t* MPU9250_getTaskHndl( void )
{
  return (TaskHandle_t*)&mpuCtrl.taskHndl;
}
