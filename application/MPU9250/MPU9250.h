/**
 ******************************************************************************
 * @file    MPU9250.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   MPU9250 header file.
 ******************************************************************************
 */

#ifndef MPU9250_MPU9250_H_
#define MPU9250_MPU9250_H_

#define USE_X_IO_MADGWICK   //!< Uncomment to use simplified Madgwick filter implementation.

#define MPU9250_DATA_LOCK()   xSemaphoreTake( MPU9250DataMutex, portMAX_DELAY )
#define MPU9250_DATA_UNLOCK() xSemaphoreGive( MPU9250DataMutex )

typedef struct
{
  float accel[3];
  float gyro[3];
  float mag[3];
  float yaw;
  float pitch;
  float roll;
  bool valid;
} T_MPU9250_OUT_DATA;

extern SemaphoreHandle_t MPU9250DataMutex;

void vTaskMPU9250( void *p );

void MPU9250_initialize( void );

void MPU9250_deactivate( void );

void MPU9250_setBeta( float beta );

void MPU9250_setZeta( float zeta );

void MPU9250_setQuaternions( void );

T_MPU9250_OUT_DATA* MPU9250_getPublicDataPtr( void );

TaskHandle_t* MPU9250_getTaskHndl( void );

#endif /* MPU9250_MPU9250_H_ */
