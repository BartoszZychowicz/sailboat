/**
 ******************************************************************************
 * @file    encoder.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Header file of encoder.
 ******************************************************************************
 */

#ifndef ENCODER_ENCODER_H_
#define ENCODER_ENCODER_H_

#define ENCODER_DATA_LOCK()   xSemaphoreTake( encoderDataMutex, portMAX_DELAY )
#define ENCODER_DATA_UNLOCK() xSemaphoreGive( encoderDataMutex )

typedef struct
{
  float angle;
  bool valid;
} T_ENCODER_OUT_DATA;

extern SemaphoreHandle_t encoderDataMutex;

void vTaskEncoder( void *p );

void ENCODER_initialize( void );

void ENCODER_deactivate( void );

T_ENCODER_OUT_DATA* ENCODER_getPublicDataPtr( void );

TaskHandle_t* ENCODER_getTaskHndl( void );

#endif /* ENCODER_ENCODER_H_ */
