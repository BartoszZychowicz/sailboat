/**
 ******************************************************************************
 * @file    encoder.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Encoder (wind direction sensor) driver.
 ******************************************************************************
 */

//========INCLUDES========

#include "appHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__); \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#define ENCODER_I2C_PORT            I2C_PORT_1
#define ENCODER_DEV_ADRESS          ( 0x36<<1 ) //!< AS5601 I2C address
#define ENCODER_DATA_PERIOD_MS      1000        //!< Time between encoder data reads

// Uncomment below to enable encoder write operations
//#define ENCODER_ENABLE_PROGRAMMING

// AS5601 register addresses
#define ENCODER_REG_CONF_H              0x07
#define ENCODER_REG_CONF_L              0x08
#define ENCODER_REG_STATUS              0x0B
#define ENCODER_REG_RAW_ANGLE_H         0x0C
#define ENCODER_REG_RAW_ANGLE_L         0x0D
#define ENCODER_REG_ANGLE_H             0x0E
#define ENCODER_REG_ANGLE_L             0x0F

// AS5601 status register masks
#define ENCODER_STAT_MASK_TOO_STRONG    0x08
#define ENCODER_STAT_MASK_TOO_WEAK      0x10
#define ENCODER_STAT_MASK_DETECTED      0x20

#define ENCODER_RES_12_BIT              4096    //<! number of states at 12 bit resolution

//========TYPES========

typedef enum
{
  ENCODER_MAG_STAT_NOT_DETECTED,    //!< No magnet detected
  ENCODER_MAG_STAT_WEAK,            //!< Magnet is too far or too weak
  ENCODER_MAG_STAT_STRONG,          //!< Magnet is too close or too strong
  ENCODER_MAG_STAT_OK,              //!< Magnet ok
  ENCODER_MAG_STAT_MAX              //!< Number of states
} T_ENCODER_MAG_STAT;

typedef enum
{
  ENCODER_ST_IDLE,
  ENCODER_ST_CHECK_MAG_STATUS,
  ENCODER_ST_READ_DATA,
  ENCODER_ST_CALCULATE,
  ENCODER_ST_UPDATE_DATA,
  ENCODER_ST_ERROR,
  ENCODER_ST_MAX
} T_ENCODER_ST;

typedef struct
{
  T_ENCODER_ST st;              //!< Encoder state machine.
  T_ENCODER_MAG_STAT magStat;   //!< Status of magnet.
  bool deactivateRequest;       //!< If set, task will suspend itself at first opportunity.
  TickType_t lastWakeTime;      //!< Time at which task was last woken.
  TaskHandle_t taskHndl;        //!< Encoder task handle.
} T_ENCODER_CTRL;

typedef struct
{
  uint16_t angle;
} T_ENCODER_READINGS;

//========STATIC VARIABLES========

static T_ENCODER_CTRL encoderCtrl;
static T_ENCODER_READINGS readings;
static T_ENCODER_OUT_DATA localOutData;
static T_ENCODER_OUT_DATA publicOutData;

//========EXPORTED VARIABLES========

SemaphoreHandle_t encoderDataMutex;

//========STATIC FUNCTIONS PROTOTYPES========

static void   ENCODER_initEncoderTask( void );

static bool   ENCODER_getMagStatus( T_ENCODER_MAG_STAT *status );

static bool   ENCODER_readAngle( uint16_t* dest );

static float  ENCODER_convertAngle( uint16_t angle );

static bool   ENCODER_readData( uint8_t regAddr, uint8_t size, uint8_t *data );

#ifdef ENCODER_ENABLE_PROGRAMMING
static bool   ENCODER_writeData( uint8_t regAddr, uint8_t *data );
#endif

static void   ENCODER_updatePublicData( void );

static void   ENCODER_invalidateOutData( void );

static void   ENCODER_setState( T_ENCODER_ST state );

//========STATIC FUNCTIONS DEFINITIONS========

static void ENCODER_initEncoderTask( void )
{
  xTaskCreate( vTaskEncoder, (const char*) "encoderTask", PLATCMN_STACK_SIZE_ENCODER, NULL, 1, &encoderCtrl.taskHndl );
  ASSERT( encoderCtrl.taskHndl );
}

static bool ENCODER_getMagStatus( T_ENCODER_MAG_STAT *status )
{
  bool result = false;
  uint8_t data = 0;

  if( ENCODER_readData( ENCODER_REG_STATUS, 1, &data ) )
  {
    if( data & ENCODER_STAT_MASK_DETECTED )
    {
      if( data & ENCODER_STAT_MASK_TOO_WEAK )
      {
        *status = ENCODER_MAG_STAT_WEAK;
      }
      else if ( data & ENCODER_STAT_MASK_TOO_STRONG )
      {
        *status = ENCODER_MAG_STAT_STRONG;
      }
      else  // magnet detected, not too weak and not too strong
      {
        *status = ENCODER_MAG_STAT_OK;
      }
    }
    else
    {
      *status = ENCODER_MAG_STAT_NOT_DETECTED;
    }

    result = true;  // read successful
  }
  else
  {
    DBGOUT( "ENCODER: Failed magnet status read!\n" );
  }
  return result;
}

static bool ENCODER_readAngle( uint16_t* dest )
{
  bool res = false;
  uint8_t rawData[2];

  if( ENCODER_readData( ENCODER_REG_ANGLE_H, 2, ( uint8_t* )&rawData ) == true )
  {
    rawData[0] = rawData[0] & 0x0F; // only bits[3:0] of ANGLE_H contain data
    *dest = ( uint16_t )( ( uint16_t )rawData[0] << 8 | rawData[1] );
    res = true;
  }
  else
  {
    DBGOUT( "ENCODER: Angle read failed\n" );
  }

  return res;
}

static float  ENCODER_convertAngle( uint16_t angle )
{
  return ( 360.0f / ENCODER_RES_12_BIT ) * ( float )angle;
}

static bool ENCODER_readData( uint8_t regAddr, uint8_t size, uint8_t *data )
{
  ASSERT(data);

  bool result = false;
  HAL_StatusTypeDef I2CResult = HAL_ERROR;

  I2C1_LOCK();  // obtain lock on I2C1 port

  I2C_readData( I2C_PORT_1, ENCODER_DEV_ADRESS, regAddr, size, data, encoderCtrl.taskHndl, &I2CResult );  //request I2C read

  ulTaskNotifyTake(pdTRUE, portMAX_DELAY ); // wait for notification from I2C task indicating request done
  I2C1_UNLOCK();  // unlock I2C1 port

  result = ( ( I2CResult == HAL_OK ) ? true : false );

  return result;
}

#ifdef ENCODER_ENABLE_PROGRAMMING
static bool ENCODER_writeData( uint8_t regAddr, uint8_t *data )
{
  ASSERT(data);

  bool result = false;
  HAL_StatusTypeDef I2CResult = HAL_ERROR;

  I2C1_LOCK();  // obtain lock on I2C1 port

  I2C_writeData( I2C_PORT_1, ENCODER_DEV_ADRESS, regAddr, 1, data, encoderCtrl.taskHndl, &I2CResult );  //request I2C read

  ulTaskNotifyTake(pdTRUE, portMAX_DELAY ); // wait for notification from I2C task indicating request done
  I2C1_UNLOCK();  // unlock I2C1 port

  result = ( ( I2CResult == HAL_OK ) ? true : false );

  return result;
}
#endif

static void ENCODER_updatePublicData( void )
{
  ENCODER_DATA_LOCK();
  memcpy( &publicOutData, &localOutData, sizeof( T_ENCODER_OUT_DATA ) );
  ENCODER_DATA_UNLOCK();
}

static void ENCODER_invalidateOutData( void )
{
  localOutData.valid = false;
  ENCODER_DATA_LOCK();
  publicOutData.valid = false;
  ENCODER_DATA_UNLOCK();
}

static void ENCODER_setState( T_ENCODER_ST state )
{
  if( encoderCtrl.st != state )
  {
    //DBGOUT("ENCODER: st %u -> %u\n", encoderCtrl.st, state );
    encoderCtrl.st = state;
  }
}

//========EXPORTED FUNCTIONS DEFINITIONS========

void vTaskEncoder( void *p )
{
  bool res = false;
  encoderCtrl.lastWakeTime = xTaskGetTickCount();   // initialize lastWakeTime at task first execution

  while(1)
  {
    switch( encoderCtrl.st )
    {
      case ENCODER_ST_IDLE:   //TODO: verify 5 times magnet strength error
        if( encoderCtrl.deactivateRequest )   //tasks suspends itself if requested
        {
          vTaskSuspend( NULL );
        }
        // TODO: verify delayUntil with task suspending
        //vTaskDelayUntil( &encoderCtrl.lastWakeTime, ( TickType_t )( ENCODER_DATA_PERIOD_MS / portTICK_PERIOD_MS ) );
        vTaskDelay( ( TickType_t )( ENCODER_DATA_PERIOD_MS / portTICK_PERIOD_MS ) );
        ENCODER_setState( ENCODER_ST_CHECK_MAG_STATUS );
        break;

      case ENCODER_ST_CHECK_MAG_STATUS:
        res = ENCODER_getMagStatus( &encoderCtrl.magStat );
        if( res == true )
        {
          if( encoderCtrl.magStat == ENCODER_MAG_STAT_OK )
          {
            ENCODER_setState( ENCODER_ST_READ_DATA );
          }
          else
          {
            ENCODER_setState( ENCODER_ST_IDLE );
          }
        }
        else
        {
          ENCODER_setState( ENCODER_ST_ERROR );
        }
        break;

      case ENCODER_ST_READ_DATA:
        if( ENCODER_readAngle( &readings.angle ) )
        {
          ENCODER_setState( ENCODER_ST_CALCULATE );
        }
        else
        {
          ENCODER_setState( ENCODER_ST_ERROR );
        }

        break;

      case ENCODER_ST_CALCULATE:
        localOutData.angle = ENCODER_convertAngle( readings.angle );
        localOutData.valid = true;
        ENCODER_setState( ENCODER_ST_UPDATE_DATA );
        break;

      case ENCODER_ST_UPDATE_DATA:
        ENCODER_updatePublicData();
        ENCODER_setState( ENCODER_ST_IDLE );
        break;

      case ENCODER_ST_ERROR:
        // TODO: error handling (reset?)
        ENCODER_invalidateOutData();
        ENCODER_setState( ENCODER_ST_IDLE ); //TODO: temp
        break;

      default:
        break;
    }
  }
}

void ENCODER_initialize( void )
{
  encoderCtrl.st = ENCODER_ST_IDLE;
  encoderCtrl.deactivateRequest = false;

  if( encoderCtrl.taskHndl )
  {
    vTaskResume( encoderCtrl.taskHndl );
  }
  else
  {
    ENCODER_initEncoderTask();
    encoderDataMutex = xSemaphoreCreateMutex();
    ASSERT( encoderDataMutex );
  }

  ENCODER_invalidateOutData();  // Consider data invalid until first successful read.
}

void ENCODER_deactivate( void )
{
  encoderCtrl.deactivateRequest = true;
}

T_ENCODER_OUT_DATA* ENCODER_getPublicDataPtr( void )
{
  return ( T_ENCODER_OUT_DATA* )&publicOutData;
}

TaskHandle_t* ENCODER_getTaskHndl( void )
{
  return (TaskHandle_t*)&encoderCtrl.taskHndl;
}
