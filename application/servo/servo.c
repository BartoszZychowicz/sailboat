/**
 ******************************************************************************
 * @file    servo.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Servo driver.
 ******************************************************************************
 */

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#include "appHeaders.h"

#define SERVO_MID(x,y)  ( ( ( x ) + ( y ) ) / 2 )

#define RUDDER_SERVO_MIN 97
#define RUDDER_SERVO_MAX 192
#define SAIL_SERVO_MIN 103
#define SAIL_SERVO_MAX 143

typedef struct
{
    uint32_t current;
    uint32_t min;
    uint32_t max;
    T_PWM_CHNL pwmChnl;
} T_SERVO_CTRL;

static T_SERVO_CTRL servoCtrl[SERVO_TYPE_MAX] =
{
  [SERVO_TYPE_RUDDER] = { SERVO_MID(RUDDER_SERVO_MIN, RUDDER_SERVO_MAX), RUDDER_SERVO_MIN, RUDDER_SERVO_MAX, PWM_CHNL_TIM4_CHNL3 },
  [SERVO_TYPE_SAIL]   = { SERVO_MID(SAIL_SERVO_MIN, SAIL_SERVO_MAX), SAIL_SERVO_MIN, SAIL_SERVO_MAX, PWM_CHNL_TIM4_CHNL4 },
};

bool SERVO_set( T_SERVO_TYPE servo, uint8_t percentValue )
{
  ASSERT(servo < SERVO_TYPE_MAX)

  bool res = false;

  if( percentValue >= 0 && percentValue <= 100 )
  {
    uint32_t range = servoCtrl[servo].max - servoCtrl[servo].min;

    servoCtrl[servo].current = servoCtrl[servo].min + ( ( percentValue * range ) / 100 );

    PWM_setCompValue( servoCtrl[servo].pwmChnl , servoCtrl[servo].current );
    DBGOUT( "SERVO: set to %u\n", servoCtrl[servo].current );
    res = true;
  }

  return res;
}
