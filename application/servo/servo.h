/**
 ******************************************************************************
 * @file    servo.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Header file of servo.
 ******************************************************************************
 */

#ifndef SERVO_SERVO_H_
#define SERVO_SERVO_H_

typedef enum
{
  SERVO_TYPE_RUDDER,
  SERVO_TYPE_SAIL,
  SERVO_TYPE_MAX
} T_SERVO_TYPE;

bool SERVO_set( T_SERVO_TYPE servo, uint8_t percentValue );

#endif /* SERVO_SERVO_H_ */
