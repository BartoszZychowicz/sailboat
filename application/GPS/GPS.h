/**
 ******************************************************************************
 * @file    GPS.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Header file of GPS.
 ******************************************************************************
 */

#ifndef GPS_GPS_H_
#define GPS_GPS_H_

#define GPS_DATA_LOCK()   xSemaphoreTake( GPSDataMutex, portMAX_DELAY )
#define GPS_DATA_UNLOCK() xSemaphoreGive( GPSDataMutex )

// Directions defines to use on bitfields
#define GPS_NORTH 1
#define GPS_SOUTH 0
#define GPS_WEST  1
#define GPS_EAST  0

typedef enum
{
  GPS_FIX_QUALITY_INVALID,
  GPS_FIX_QUALITY_GPS_FIX,
  GPS_FIX_QUALITY_DGPS_FIX,
} T_GPS_FIX_QUALITY;

typedef struct
{
    float latitude;
    float longitude;
    uint8_t latitudeSign; // N/S
    uint8_t longitudeSign; //W/E
    T_GPS_FIX_QUALITY fixQuality;
    bool valid;
} T_GPS_OUT_DATA;

extern SemaphoreHandle_t GPSDataMutex;

void GPS_initialize( void );

void GPS_deactivate( void );

bool GPS_parseSentence( uint8_t *rawData );

void vTaskGPS( void *p );

T_GPS_OUT_DATA* GPS_getPublicDataPtr( void );

TaskHandle_t* GPS_getTaskHndl( void );

#endif /* GPS_GPS_H_ */
