/**
 ******************************************************************************
 * @file    GPS.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   GPS driver.
 ******************************************************************************
 */


//========INCLUDES========

#include "appHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

//#define GPS_FULL_DEBUG                      //!< Uncomment to get full debug messages on GPS module
//#define GPS_STATE_DEBUG                     //!< Uncomment to get state machine debug

#define GPS_MAX_SENTENCE_FIELD_SIZE 20        //!< Max size of single field in NMEA sentence
#define GPS_MAX_SENTENCE_SIZE 82              //!< Max NMEA 0183 sentence size is 82 chars
#define GPS_SENTENCE_START '$'                //!< Each NMEA 0183 sentence starts with $ sign
#define GPS_FIELD_DELIMITER ','               //!< Field separator in NMEA 0183
#define GPS_SENTENCE_END_1  PLATCMN_ASCII_CR  //!< First character of sentence end
#define GPS_SENTENCE_END_2  PLATCMN_ASCII_LF  //!< Second character of sentence end
#define GPS_FIELD_GPGGA_TIMESTAMP_LEN 10      //!< HHMMSS.00 format + closing NULL char
#define GPS_NMEA_0183_SENTENCE_ID_LEN 7       //!< Sentence ID length with following comma, e.g. $GPGGA,
#define GPS_TIMEOUT_MS 5000                   //!< After this time[ms] without new data, current data is marked invalid.

//========TYPES========

typedef enum
{
  GPS_NMEA_SENT_TYPE_GPGGA,
  GPS_NMEA_SENT_TYPE_GPGLL,
  GPS_NMEA_SENT_TYPE_GPRMC,
  GPS_NMEA_SENT_TYPE_GPVTG,
  GPS_NMEA_SENT_TYPE_GPGSA,
  GPS_NMEA_SENT_TYPE_GPGSV,
  GPS_NMEA_SENT_TYPE_MAX,
  GPS_NMEA_SENT_TYPE_NONE // After TYPE_MAX so it's not iterated over when checking sentence type.
} T_GPS_NMEA_SENT_TYPE;

typedef enum
{
  GPS_ST_IDLE,
  GPS_ST_SENTENCE_RECEIVING,
  GPS_ST_SENTENCE_PARSING,
  GPS_ST_UPDATING_DATA,
  GPS_ST_DONE,
  GPS_ST_ERROR,
  GPS_ST_MAX
} T_GPS_ST;

/*
 * @brief Structure for common part of all sentence structures
 */
typedef struct
{
    T_GPS_NMEA_SENT_TYPE sentType;
    uint8_t* data;
} T_GPS_NMEA_SENTENCE_COMMON;

/*
 * @brief   NMEA 0183 GPGGA sentence data structure.
 */
typedef struct
{
    T_GPS_NMEA_SENT_TYPE sentType;  //!< Sentence type
    float latitude;                 //!< Latitude coordinate
    float longitude;                //!< Longitude coordinate
    uint8_t latitudeSign;           //!< Sign of latitude - N/S
    uint8_t longitudeSign;          //!< Sign of longitude - W/E
    uint8_t satellites;             //!< Number of satellites in sight
    T_GPS_FIX_QUALITY fixQuality;   //!< GPS fix quality
    uint8_t time[GPS_FIELD_GPGGA_TIMESTAMP_LEN];  //!< Time, HHMMSS
} T_GPS_NMEA_SENTENCE_GPGGA;

typedef struct
{
    TickType_t prevReadTime;
    T_GPS_ST state;
    uint8_t rcvdChars;
    bool deactivateRequest;
    TaskHandle_t taskHndl;
} T_GPS_CTRL;

typedef union
{
    T_GPS_NMEA_SENTENCE_COMMON cmn;
    T_GPS_NMEA_SENTENCE_GPGGA GPGGA;
} T_GPS_NMEA_SENTENCE;

//========STATIC VARIABLES========

static T_GPS_CTRL gpsCtrl;
static T_GPS_NMEA_SENTENCE gpsSentence;
static T_GPS_OUT_DATA publicOutData;
static uint8_t sentenceBuffer[GPS_MAX_SENTENCE_SIZE+1] = {0};
/*
 * @brief NMEA 0183 sentence identifiers.
 */
static const char * sentenceIDs[GPS_NMEA_SENT_TYPE_MAX] =
{
  [GPS_NMEA_SENT_TYPE_GPGGA] = "$GPGGA,",
  [GPS_NMEA_SENT_TYPE_GPGLL] = "$GPGLL,",
  [GPS_NMEA_SENT_TYPE_GPRMC] = "$GPRMC,",
  [GPS_NMEA_SENT_TYPE_GPVTG] = "$GPVTG,",
  [GPS_NMEA_SENT_TYPE_GPGSA] = "$GPGSA,",
  [GPS_NMEA_SENT_TYPE_GPGSV] = "$GPGSV,"
};

//========EXPORTED VARIABLES========

SemaphoreHandle_t GPSDataMutex;

//========STATIC FUNCTIONS PROTOTYPES========

static void GPS_initGPSTask( void );

static T_GPS_NMEA_SENT_TYPE GPS_getSentenceType( uint8_t *buf );

static uint8_t GPS_readFieldUntilDelimiter( uint8_t *src, uint8_t *dest );

static void GPS_parseSentenceGPGGA( uint8_t *rawData, T_GPS_NMEA_SENTENCE *sentence );

static bool GPS_checkTimeout( void );

static void GPS_updatePublicData( void );

static void GPS_invalidateOutData( void );

static void GPS_setState( T_GPS_ST state );

#ifdef DEBUG
static void GPS_printSentence( T_GPS_NMEA_SENTENCE *sentencePtr );
#else
#define GPS_printSentence(x)
#endif /* DEBUG */

//========STATIC FUNCTIONS DEFINITIONS========

static void GPS_initGPSTask( void )
{
  xTaskCreate( vTaskGPS, (const char*) "GPSTask", PLATCMN_STACK_SIZE_GPS, NULL, 1, &gpsCtrl.taskHndl );
  ASSERT( gpsCtrl.taskHndl );
}

static T_GPS_NMEA_SENT_TYPE GPS_getSentenceType( uint8_t *buf )
{
  ASSERT(buf);

  uint8_t i = 0;
  char *sentence = NULL;

  for( i = 0; i < GPS_NMEA_SENT_TYPE_MAX; i++ )
  {
    sentence = strstr( (char*)buf, sentenceIDs[i]);
    if(sentence)
    {
      return (T_GPS_NMEA_SENT_TYPE)i;
    }
  }
  return GPS_NMEA_SENT_TYPE_NONE;
}

static uint8_t GPS_readFieldUntilDelimiter( uint8_t *src, uint8_t *dest )
{
  ASSERT(src)
  ASSERT(dest)

  uint8_t charsRead = 0;
  while( charsRead < GPS_MAX_SENTENCE_FIELD_SIZE )
  {
    charsRead++;
    if( *src != GPS_FIELD_DELIMITER )
    {
      *dest++ = *src++;
    }
    else
    {
      break;
    }
  }
  return charsRead;
}

static void GPS_parseSentenceGPGGA( uint8_t *rawData, T_GPS_NMEA_SENTENCE *sentence )
{
  ASSERT(rawData);
  ASSERT(sentence);

  uint8_t temp[GPS_MAX_SENTENCE_FIELD_SIZE] = {0};
  uint8_t dataRead = 0;
  uint8_t *dataPtr = rawData;

  // Parse time
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, ( uint8_t* )&sentence->GPGGA.time );
  dataPtr += dataRead;

  // Parse latitude
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, (uint8_t*)&temp );
  dataPtr += dataRead;
  if( dataRead > 1 )  // Some actual data was read, not only delimiter
  {
    sentence->GPGGA.latitude = strtof( ( char* )&temp, NULL );
  }
  memset( &temp, 0, PLATCMN_ARRAY_MEMBER_COUNT(temp) ); // clear buffer

  // Parse latitude sign N/S
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, ( uint8_t* )&sentence->GPGGA.latitudeSign );
  dataPtr += dataRead;

  // Parse longitude
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, (uint8_t*)&temp );
  dataPtr += dataRead;
  if( dataRead > 1 )  // Some actual data was read, not only delimiter
  {
    sentence->GPGGA.longitude = strtof( ( char* )&temp, NULL );
  }
  memset( &temp, 0, PLATCMN_ARRAY_MEMBER_COUNT(temp) ); // clear buffer

  // Parse longitude sign W/E
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, ( uint8_t* )&sentence->GPGGA.longitudeSign );
  dataPtr += dataRead;

  // Parse fix quality
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, (uint8_t*)&temp );
  dataPtr += dataRead;
  sentence->GPGGA.fixQuality = GPS_FIX_QUALITY_INVALID;

  if( dataRead == 2 )  // Fix quality should be single digit + delimiter char
  {
    switch( temp[0] )
    {
      case '1':
        sentence->GPGGA.fixQuality = GPS_FIX_QUALITY_GPS_FIX;
        break;

      case '2':
        sentence->GPGGA.fixQuality = GPS_FIX_QUALITY_DGPS_FIX;
        break;

      default:
        break;
    }
  }
  memset( &temp, 0, PLATCMN_ARRAY_MEMBER_COUNT(temp) ); // clear buffer

  // Parse satellites
  dataRead = GPS_readFieldUntilDelimiter( dataPtr, (uint8_t*)&temp );
  dataPtr += dataRead;
  if( dataRead > 1 )  // Some actual data was read, not only delimiter
  {
    sentence->GPGGA.satellites = (uint8_t)strtoul( ( char* )&temp, NULL, 10 );
  }
  memset( &temp, 0, PLATCMN_ARRAY_MEMBER_COUNT(temp) ); // clear buffer
}

static bool GPS_checkTimeout( void )
{
  bool res = true;          // default result is timeout
  TickType_t now = 0;       // current time
  TickType_t timeDiff = 0;  // time since last successful read

  now = xTaskGetTickCount();

  if( now >= gpsCtrl.prevReadTime )
  {
    timeDiff = now - gpsCtrl.prevReadTime;
  }
  else  // Tick counter overflowed
  {
    timeDiff = ( UINT32_MAX - gpsCtrl.prevReadTime ) + now;
  }

  if( ( timeDiff * portTICK_PERIOD_MS ) < GPS_TIMEOUT_MS )  // if time converted to ms less than timeout
  {
    res = false;
  }

  return res;
}

static void GPS_updatePublicData( void )
{
  GPS_DATA_LOCK();
  publicOutData.longitude = gpsSentence.GPGGA.longitude;
  publicOutData.longitudeSign = gpsSentence.GPGGA.longitudeSign;
  publicOutData.latitude = gpsSentence.GPGGA.latitude;
  publicOutData.latitudeSign = gpsSentence.GPGGA.latitudeSign;
  publicOutData.fixQuality = gpsSentence.GPGGA.fixQuality;
  publicOutData.valid = true;
  GPS_DATA_UNLOCK();
}

static void GPS_invalidateOutData( void )
{
  GPS_DATA_LOCK();
  publicOutData.valid = false;
  GPS_DATA_UNLOCK();
}

static void GPS_setState( T_GPS_ST state )
{
  if( gpsCtrl.state != state )
  {
#ifdef GPS_STATE_DEBUG
    DBGOUT("GPS: st %u -> %u\n", gpsCtrl.state, state );
#endif
    gpsCtrl.state = state;
  }
}

#ifdef DEBUG

static void GPS_printSentence( T_GPS_NMEA_SENTENCE *sentencePtr )
{
  T_GPS_NMEA_SENTENCE *sentence = ( T_GPS_NMEA_SENTENCE* )sentencePtr;

  switch( sentence->cmn.sentType )
  {
    case GPS_NMEA_SENT_TYPE_GPGGA:
      DBGOUT("Type: $GPGGA\n"
             "GPS fix: %s\n"
             "Time: %s\n"
             "Latitude: %.3f%c\n"
             "Longitude: %.3f%c\n"
             "Satellites: %u\n\n",
             ( sentence->GPGGA.fixQuality == GPS_FIX_QUALITY_INVALID ? "Invalid" : "OK" ),
             sentence->GPGGA.time,
             sentence->GPGGA.latitude, sentence->GPGGA.latitudeSign,
             sentence->GPGGA.longitude, sentence->GPGGA.longitudeSign,
             sentence->GPGGA.satellites );
      break;

    case GPS_NMEA_SENT_TYPE_GPGLL:
    case GPS_NMEA_SENT_TYPE_GPGSA:
    case GPS_NMEA_SENT_TYPE_GPRMC:
    case GPS_NMEA_SENT_TYPE_GPVTG:
    case GPS_NMEA_SENT_TYPE_GPGSV:
      DBGOUT("%s sentence\n", sentenceIDs[sentence->cmn.sentType]);
      break;

    default:
      DBGOUT("GPS: Invalid sentence\n");
      break;
  }
}

#endif /* DEBUG */


//========EXPORTED FUNCTIONS DEFINITIONS========

bool GPS_parseSentence( uint8_t *rawData )
{
  bool result = false;
  T_GPS_NMEA_SENTENCE *sentence = &gpsSentence;

  sentence->cmn.sentType = GPS_getSentenceType( rawData );

  if( sentence->cmn.sentType != GPS_NMEA_SENT_TYPE_NONE ) // if valid sentence type, parse it
  {
    rawData += GPS_NMEA_0183_SENTENCE_ID_LEN; // TODO: Skip over sentence ID, parsers only process data

#ifdef GPS_FULL_DEBUG
        GPS_printSentence( sentence );
#endif

    switch( sentence->cmn.sentType )
    {
      case GPS_NMEA_SENT_TYPE_GPGGA:
        GPS_parseSentenceGPGGA( rawData, sentence );
        break;

      default:
        break;
    }

    result = true;
  }

    return result;
}

void GPS_initialize( void )
{
  gpsCtrl.state = GPS_ST_IDLE;
  gpsCtrl.deactivateRequest = false;
  if( gpsCtrl.taskHndl )  //task already created => GPS is reinitialized
  {
    vTaskResume( gpsCtrl.taskHndl );
    UART_enable( UART_PORT_GPS );
  }
  else  // No task created yet => GPS first initialization
  {
    GPS_initGPSTask();
    UART_registerTaskHndl( UART_PORT_GPS, gpsCtrl.taskHndl );
    GPSDataMutex = xSemaphoreCreateMutex();
    ASSERT( GPSDataMutex );
  }
  GPS_invalidateOutData(); // Consider data invalid until first successful read.
}

void GPS_deactivate( void )
{
  gpsCtrl.deactivateRequest = true;
  UART_disable( UART_PORT_GPS );
}

void vTaskGPS( void *p )
{
  uint16_t dataCount = 0;
  uint8_t rcvdChar = 0;

  while( 1 )
  {
    switch( gpsCtrl.state )
    {
      case GPS_ST_IDLE:   //TODO: consider receiving notification from ISR here
        if( gpsCtrl.deactivateRequest )   //tasks suspends itself if requested
        {
          vTaskSuspend( NULL );
        }
        if( GPS_checkTimeout() && publicOutData.valid == true ) // timeout is only significant if data is valid
        {
          DBGOUT( "GPS: timeout\n" );
          GPS_setState( GPS_ST_ERROR );
        }
        dataCount = UART_getRcvdDataCount( UART_PORT_GPS ); //check if new data received from GPS
        if( dataCount == 0 )
        {
          UART_setTaskNotifications( UART_PORT_GPS, true );
          ulTaskNotifyTake( pdTRUE, portMAX_DELAY );  // Wait until new GPS data arrives
        }
        else
        {
          while( dataCount-- > 0)
          {
            UART_readData( UART_PORT_GPS, &rcvdChar, 1 );
            if( rcvdChar == GPS_SENTENCE_START )
            {
            sentenceBuffer[gpsCtrl.rcvdChars++] = rcvdChar;
            GPS_setState( GPS_ST_SENTENCE_RECEIVING );
            break;
            }
          }
        }
        break;

      case GPS_ST_SENTENCE_RECEIVING:
        dataCount = UART_getRcvdDataCount( UART_PORT_GPS );
        while( dataCount-- > 0)
        {
          UART_readData( UART_PORT_GPS, &rcvdChar , 1 );
          sentenceBuffer[gpsCtrl.rcvdChars++] = rcvdChar;

          if( rcvdChar == GPS_SENTENCE_END_2 )
          {
            // Check if char before current was CR
            if( sentenceBuffer[gpsCtrl.rcvdChars - 2] == GPS_SENTENCE_END_1 )
            {
              GPS_setState( GPS_ST_SENTENCE_PARSING );
            }
            else
            {
              DBGOUT("GPS: Invalid sentence ending received\n");
              GPS_setState( GPS_ST_ERROR );   // Invalid sentence (no CR LF at the end)
            }
            break;
          }

          if( gpsCtrl.rcvdChars > GPS_MAX_SENTENCE_SIZE )
          {
            DBGOUT("GPS: Too long sentence\n");
            GPS_setState( GPS_ST_ERROR ); // Invalid sentence (too long)
            break;
          }
        }
        break;

      case GPS_ST_SENTENCE_PARSING:
        if ( GPS_parseSentence( (uint8_t*)&sentenceBuffer ) )
        {
          if( gpsSentence.cmn.sentType == GPS_NMEA_SENT_TYPE_GPGGA )  // Public data updated only from GPGGA sentences
          {
            GPS_setState( GPS_ST_UPDATING_DATA );
          }
          else
          {
            GPS_setState( GPS_ST_DONE );
          }
        }
        else
        {
          GPS_setState( GPS_ST_ERROR );
        }
        break;

      case GPS_ST_UPDATING_DATA:
        GPS_updatePublicData();
        gpsCtrl.prevReadTime = xTaskGetTickCount(); //Set new successful read timestamp
        GPS_setState( GPS_ST_DONE );
        break;

      case GPS_ST_DONE:
        gpsCtrl.rcvdChars = 0;
        GPS_setState( GPS_ST_IDLE );
        break;

      case GPS_ST_ERROR:
        DBGOUT("GPS: Error state\n");
        GPS_invalidateOutData();
        //TODO: consider flushing gps UART buffer, resetting gps
        GPS_setState( GPS_ST_DONE );
        break;

      default:
        break;
    }
  }
}

T_GPS_OUT_DATA* GPS_getPublicDataPtr( void )
{
  return ( T_GPS_OUT_DATA* )&publicOutData;
}

TaskHandle_t* GPS_getTaskHndl( void )
{
  return ( TaskHandle_t* )&gpsCtrl.taskHndl;
}
