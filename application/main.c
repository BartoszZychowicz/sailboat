/**
 ******************************************************************************
 * @file    main.c
 * @author 	Bartosz Zychowicz
 * @version
 * @date
 * @brief   Sailboat project main.
 ******************************************************************************
 */

//========INCLUDES========

#include "appHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

//========STATIC VARIABLES========
//========STATIC FUNCTIONS PROTOTYPES========

static void MAIN_platformInitialize( void );

static void MAIN_applicationInitialize( void );

//========STATIC FUNCTIONS DEFINITIONS========

static void MAIN_platformInitialize( void )
{
  //HAL_Init();
  //RCC_initSystemClock();  //TODO: verify if it works correctly

  SystemCoreClockUpdate();
  GPIO_initialize();
  UART_initialize();
  I2C_initialize();
  PWM_initialize();
}

static void MAIN_applicationInitialize( void )
{
  ENCODER_initialize();
  MPU9250_initialize();
  DEBUG_initialize();
  GPS_initialize();
  DATAMGMT_initialize();
}



//========EXPORTED FUNCTIONS DEFINITIONS========

int main( void )
{

	MAIN_platformInitialize();
	MAIN_applicationInitialize();

	vTaskStartScheduler();

	//should never reach here
	return 0;
}
