/**
 ******************************************************************************
 * @file    appCmn.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Application common header file.
 ******************************************************************************
 */

#ifndef APPCMN_H_
#define APPCMN_H_

#define PI M_PI

#endif /* APPCMN_H_ */
