/**
 ******************************************************************************
 * @file    dataMgmt.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Data management module
 ******************************************************************************
 */

//========INCLUDES========

#include "appHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...)   DBG_LOCK(); \
                      printf(__VA_ARGS__);  \
                      DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

//Uncomment to periodically print to DBG port readings from all sensors
//#define DATAMGMT_SENSOR_DEMO

#define DATAMGMT_OUTPREP_BUF_SIZE sizeof( T_DATAMGMT_MSG_O )
#define DATAMGMT_OUT_BUF_SIZE     ( ( 2 * DATAMGMT_OUTPREP_BUF_SIZE ) + 2 ) // Worst case each byte escaped + start and end sign
#define DATAMGMT_IN_BUF_SIZE      64

#define DATAMGMT_FRAME_PREFIX_SIZE  2   // Type(1 byte) + payload length(1 byte)
#define DATAMGMT_FRAME_POSTFIX_SIZE 1   // CRC8(1 byte)
#define DATAMGMT_FRAME_SIZE ( DATAMGMT_FRAME_PREFIX_SIZE + DATAMGMT_FRAME_POSTFIX_SIZE )  // Full frame (no payload) size

//========TYPES========

typedef enum
{
  DATAMGMT_ST_IDLE,
  DATAMGMT_ST_MSG_RECEIVING,
  DATAMGMT_ST_MSG_VERIFY,
  DATAMGMT_ST_CMD_EXECUTE,
  DATAMGMT_ST_TRANSMITTING,
  DATAMGMT_ST_DONE,
  DATAMGMT_ST_ERROR,
  DATAMGMT_ST_MAX
} T_DATAMGMT_ST;

typedef struct
{
    uint8_t rcvdCount;
    T_DATAMGMT_ST state;
    TaskHandle_t taskHndl;
    bool signEscaped;
    bool read;
} T_DATAMGMT_CTRL;

typedef enum
{
  DATAMGMT_MSG_TYPE_O_ACK               =   0x10,
  DATAMGMT_MSG_TYPE_O_SENSOR_BASIC      =   0x11,
  DATAMGMT_MSG_TYPE_O_SENSOR_EXT_MPU    =   0x12,
  DATAMGMT_MSG_TYPE_O_SENSOR_EXT_GPS    =   0x13,
  DATAMGMT_MSG_TYPE_O_SENSOR_EXT_ENCD   =   0x14
} T_DATAMGMT_MSG_TYPE_O;

typedef enum
{
  DATAMGMT_MSG_TYPE_I_REQ_SENSOR_BASIC  =   0x60,
  DATAMGMT_MSG_TYPE_I_SET_SERVOS        =   0x61
} T_DATAMGMT_MSG_TYPE_I;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_O type;
    uint8_t payloadLen;
    uint8_t* data;
} T_DATAMGMT_MSG_O_CMN;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_O type;
    uint8_t payloadLen;
    uint8_t checksum;
} T_DATAMGMT_MSG_O_ACK;

typedef struct
{
    uint8_t longitudeSign : 1;
    uint8_t latitudeSign  : 1;
    uint8_t mpuValid      : 1;
    uint8_t gpsValid      : 1;
    uint8_t encdValid     : 1;
} T_DATAMGMT_MSG_O_SENSOR_BASIC_INFOMASK;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_O type;
    uint8_t payloadLen;
    float mpuYaw;
    float mpuPitch;
    float mpuRoll;
    float gpsLongitude;
    float gpsLatitude;
    float encoderAngle;
    T_DATAMGMT_MSG_O_SENSOR_BASIC_INFOMASK infoMask;
    uint8_t checksum;
} T_DATAMGMT_MSG_O_SENSOR_BASIC;

typedef struct PACKED   //TODO: use MPU_outData
{
    T_DATAMGMT_MSG_TYPE_O type;
    uint8_t payloadLen;
    float accel[3];
    float gyro[3];
    float mag[3];
    float yaw;
    float pitch;
    float roll;
    uint8_t infoMask;
    uint8_t checksum;
} T_DATAMGMT_MSG_O_SENSOR_EXT_MPU;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_O type;
    uint8_t payloadLen;
    float longitude;
    float latitude;
    uint8_t satellites;
    uint8_t infoMask;
    uint8_t checksum;
} T_DATAMGMT_MSG_O_SENSOR_EXT_GPS;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_O type;
    uint8_t payloadLen;
    float angle;
    uint8_t infoMask;
    uint8_t checksum;
} T_DATAMGMT_MSG_O_SENSOR_EXT_ENCD;

typedef union
{
    T_DATAMGMT_MSG_O_CMN              common;
    T_DATAMGMT_MSG_O_ACK              ack;
    T_DATAMGMT_MSG_O_SENSOR_BASIC     sensorBsc;
    T_DATAMGMT_MSG_O_SENSOR_EXT_MPU   extMPU;
    T_DATAMGMT_MSG_O_SENSOR_EXT_GPS   extGPS;
    T_DATAMGMT_MSG_O_SENSOR_EXT_ENCD  extENCD;
} T_DATAMGMT_MSG_O;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_I type;
    uint8_t payloadLen;
    uint8_t* data;
} T_DATAMGMT_MSG_I_CMN;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_I type;
    uint8_t payloadLen;
    uint8_t checksum;
} T_DATAMGMT_MSG_I_REQ_SENSOR_BASIC;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE_I type;
    uint8_t payloadLen;
    uint8_t sailServo;
    uint8_t rudderServo;
    uint8_t checksum;
} T_DATAMGMT_MSG_I_SET_SERVOS;

typedef union
{
    T_DATAMGMT_MSG_I_CMN              common;
    T_DATAMGMT_MSG_I_REQ_SENSOR_BASIC reqSensorBasic;
    T_DATAMGMT_MSG_I_SET_SERVOS       setServos;
} T_DATAMGMT_MSG_I;

typedef union
{
    T_DATAMGMT_MSG_TYPE_I inMsg;
    T_DATAMGMT_MSG_TYPE_O outMsg;
} T_DATAMGMT_MSG_TYPE;

typedef struct PACKED
{
    T_DATAMGMT_MSG_TYPE type;
    uint8_t payloadLen;
    uint8_t* data;
} T_DATAMGMT_MSG_CMN;

typedef union
{
    T_DATAMGMT_MSG_CMN  cmn;
    T_DATAMGMT_MSG_O    out;
    T_DATAMGMT_MSG_I    in;
} T_DATAMGMT_MSG;

//========STATIC VARIABLES========

static T_DATAMGMT_CTRL dataMgmtCtrl;

static uint8_t outputPrepBuf[DATAMGMT_OUTPREP_BUF_SIZE] = {0};

static uint8_t outputBuf[DATAMGMT_OUT_BUF_SIZE] = {0};

static uint8_t inputBuf[DATAMGMT_IN_BUF_SIZE] = {0};


//========EXPORTED VARIABLES========
//========STATIC FUNCTIONS PROTOTYPES========

static void DATAMGMT_initDataMgmtTask( void );

static void DATAMGMT_prepareSensorBasicMsg( T_DATAMGMT_MSG_O *frame );

static void DATAMGMT_prepareAckMsg( T_DATAMGMT_MSG_O *frame );

static void DATAMGMT_sendFrame( T_DATAMGMT_MSG_O *frame );

static uint8_t DATAMGMT_calcFrameChecksum( T_DATAMGMT_MSG *frame );

static uint8_t DATAMGMT_byteStuffData( uint8_t *input, uint8_t *output, uint8_t size );

static bool DATAMGMT_verifyInMessage( T_DATAMGMT_MSG_I *frame );

static bool DATAMGMT_dispatch( T_DATAMGMT_MSG_I *frame );

static void DATAMGMT_executeSetServos( T_DATAMGMT_MSG_I *frame );

#ifdef DATAMGMT_SENSOR_DEMO
static void DATAMGMT_getEncoderData( void );

static void DATAMGMT_getGPSData( void );

static void DATAMGMT_getMPU9250Data( void );
#endif

static void DATAMGMT_setState( T_DATAMGMT_ST state );

//========STATIC FUNCTIONS DEFINITIONS========

static void DATAMGMT_initDataMgmtTask( void )
{
  xTaskCreate( vTaskDataMgmt, (const char*) "DataMgmtTask", PLATCMN_STACK_SIZE_DATAMGMT, NULL, 1, &dataMgmtCtrl.taskHndl );
  ASSERT( dataMgmtCtrl.taskHndl );
}

static void DATAMGMT_prepareSensorBasicMsg( T_DATAMGMT_MSG_O *frame )
{
  T_MPU9250_OUT_DATA* mpuDataPtr = NULL;
  T_GPS_OUT_DATA* gpsDataPtr = NULL;
  T_ENCODER_OUT_DATA* encdDataPtr = NULL;

  frame->common.type = DATAMGMT_MSG_TYPE_O_SENSOR_BASIC;
  frame->common.payloadLen = sizeof( T_DATAMGMT_MSG_O_SENSOR_BASIC ) - DATAMGMT_FRAME_SIZE;

  //get MPU data
  MPU9250_DATA_LOCK();
  mpuDataPtr = MPU9250_getPublicDataPtr();
  if( mpuDataPtr->valid )
  {
    frame->sensorBsc.mpuYaw = mpuDataPtr->yaw;
    frame->sensorBsc.mpuPitch = mpuDataPtr->pitch;
    frame->sensorBsc.mpuRoll = mpuDataPtr->roll;
    frame->sensorBsc.infoMask.mpuValid = true;
    DBGOUT( "MPU: Y: %f, P: %f, R: %f\n", mpuDataPtr->yaw, mpuDataPtr->pitch, mpuDataPtr->roll );
  }
  else
  {
    frame->sensorBsc.infoMask.mpuValid = false;
    DBGOUT( "MPU: Invalid\n" );
  }
  MPU9250_DATA_UNLOCK();

  //get GPS data
  GPS_DATA_LOCK();
  gpsDataPtr = GPS_getPublicDataPtr();
  if( gpsDataPtr->valid )
  {
    frame->sensorBsc.infoMask.gpsValid = true;
    frame->sensorBsc.gpsLatitude = gpsDataPtr->latitude;
    frame->sensorBsc.gpsLongitude = gpsDataPtr->longitude;
    switch( gpsDataPtr->latitudeSign )
    {
      case 'N':
        frame->sensorBsc.infoMask.latitudeSign = GPS_NORTH;
        break;
      case 'S':
        frame->sensorBsc.infoMask.latitudeSign = GPS_SOUTH;
        break;
      default:
        frame->sensorBsc.infoMask.gpsValid = false;
        break;
    }

    switch( gpsDataPtr->longitudeSign )
    {
      case 'E':
        frame->sensorBsc.infoMask.longitudeSign = GPS_EAST;
        break;
      case 'W':
        frame->sensorBsc.infoMask.longitudeSign = GPS_WEST;
        break;
      default:
        frame->sensorBsc.infoMask.gpsValid = false;
        break;
    }
    DBGOUT( "GPS: fix: %s\tLat: %.3f%c\tLon: %.3f%c\n",
                 ( gpsDataPtr->fixQuality == GPS_FIX_QUALITY_INVALID ? "Invalid" : "OK" ),
                 gpsDataPtr->latitude, gpsDataPtr->latitudeSign,
                 gpsDataPtr->longitude, gpsDataPtr->longitudeSign );
  }
  else
  {
    frame->sensorBsc.infoMask.gpsValid = false;
    DBGOUT( "GPS: Invalid\n" );
  }
  GPS_DATA_UNLOCK();

  //get encoder data
  ENCODER_DATA_LOCK();
  encdDataPtr = ENCODER_getPublicDataPtr();
  if( encdDataPtr->valid )
  {
    frame->sensorBsc.encoderAngle = encdDataPtr->angle;
    frame->sensorBsc.infoMask.encdValid = true;
    DBGOUT( "ENC: %.3f\n", encdDataPtr->angle );
  }
  else
  {
    frame->sensorBsc.infoMask.encdValid = false;
    DBGOUT( "ENC: Invalid\n" );
  }
  ENCODER_DATA_UNLOCK();

  frame->sensorBsc.checksum = DATAMGMT_calcFrameChecksum( ( T_DATAMGMT_MSG* )frame );
}

static void DATAMGMT_prepareAckMsg( T_DATAMGMT_MSG_O *frame )
{
  frame->common.type = DATAMGMT_MSG_TYPE_O_ACK;
  frame->common.payloadLen = sizeof( T_DATAMGMT_MSG_O_ACK ) - DATAMGMT_FRAME_SIZE;
  frame->ack.checksum = DATAMGMT_calcFrameChecksum( ( T_DATAMGMT_MSG* )frame );
}

static void DATAMGMT_sendFrame( T_DATAMGMT_MSG_O *frame )
{
  uint8_t tempSize = 0;
  uint8_t dataSize = 0;
  uint8_t* bufPtr = ( uint8_t* )&outputBuf;
  // start sign to buffer
  *bufPtr++ = UART_CTRLPORT_FRAME_START;
  dataSize++;
  // frame to buffer
  tempSize = DATAMGMT_byteStuffData( ( uint8_t* )&frame->common.type, bufPtr, ( DATAMGMT_FRAME_SIZE + frame->common.payloadLen ) );
  dataSize += tempSize;
  bufPtr += tempSize;
  // end sign to buffer
  *bufPtr++ = UART_CTRLPORT_FRAME_END;
  dataSize++;

  UART_sendData( UART_PORT_CTRL, ( uint8_t* )&outputBuf, dataSize );
}

static uint8_t DATAMGMT_calcFrameChecksum( T_DATAMGMT_MSG *frame )
{
  return CRC8_calcChecksum( ( uint8_t* )frame, ( size_t )( DATAMGMT_FRAME_PREFIX_SIZE + frame->cmn.payloadLen ) );
}

static uint8_t DATAMGMT_byteStuffData( uint8_t *input, uint8_t *output, uint8_t size )
{
  uint8_t outputSize = 0;
  while( size-- )
  {
    if( *input == UART_CTRLPORT_FRAME_START || *input == UART_CTRLPORT_FRAME_END )
    {
      *output++ = UART_CTRLPORT_FRAME_ESC_SIGN;
      outputSize++;
    }
    *output++ = *input++;
    outputSize++;
  }
  return outputSize;
}

static bool DATAMGMT_verifyInMessage( T_DATAMGMT_MSG_I *frame )
{
  uint8_t crcRcvd = 0;
  bool res = false;

  if( frame->common.payloadLen == ( dataMgmtCtrl.rcvdCount - DATAMGMT_FRAME_SIZE ) )
  {
    switch( frame->common.type )
    {
      case DATAMGMT_MSG_TYPE_I_REQ_SENSOR_BASIC:
        crcRcvd = frame->reqSensorBasic.checksum;
        break;

      case DATAMGMT_MSG_TYPE_I_SET_SERVOS:
        crcRcvd = frame->setServos.checksum;
        break;

      default:
        DBGOUT( "DATAMGMT: Unknown command %02x\n", frame->common.type );
        break;
    }
    if( crcRcvd == DATAMGMT_calcFrameChecksum( ( T_DATAMGMT_MSG* )frame ) )
    {
      res = true;
    }
  }
  return res;
}

static bool DATAMGMT_dispatch( T_DATAMGMT_MSG_I *frame )
{
  switch( frame->common.type )
  {
    case DATAMGMT_MSG_TYPE_I_REQ_SENSOR_BASIC:
      DATAMGMT_prepareSensorBasicMsg( ( T_DATAMGMT_MSG_O* )&outputPrepBuf );
      break;

    case DATAMGMT_MSG_TYPE_I_SET_SERVOS:
      DATAMGMT_executeSetServos( frame );
      DATAMGMT_prepareAckMsg( ( T_DATAMGMT_MSG_O* )&outputPrepBuf );
      break;

    default:
      DBGOUT( "DATAMGMT: Dispatcher unknown cmd %02x\n", frame->common.type );
      break;
  }
  return true;  //TODO: return status
}

static void DATAMGMT_executeSetServos( T_DATAMGMT_MSG_I *frame )
{
  SERVO_set( SERVO_TYPE_RUDDER, frame->setServos.rudderServo );
  SERVO_set( SERVO_TYPE_SAIL, frame->setServos.sailServo );
}

#ifdef DATAMGMT_SENSOR_DEMO
static void DATAMGMT_getEncoderData( void )
{
  T_ENCODER_OUT_DATA* dataPtr = NULL;

  ENCODER_DATA_LOCK();
  dataPtr = ENCODER_getPublicDataPtr();
  if( dataPtr->valid )
  {
    DBGOUT( "ENC: %.3f\n", dataPtr->angle );
  }
  else
  {
    DBGOUT( "ENC: Invalid\n" );
  }
  ENCODER_DATA_UNLOCK();
}

static void DATAMGMT_getGPSData( void )
{
  T_GPS_OUT_DATA* dataPtr = NULL;

  GPS_DATA_LOCK();
  dataPtr = GPS_getPublicDataPtr();
  if( dataPtr->valid )
  {
    DBGOUT( "GPS: fix: %s\tLat: %.3f%c\tLon: %.3f%c\n",
                 ( dataPtr->fixQuality == GPS_FIX_QUALITY_INVALID ? "Invalid" : "OK" ),
                 dataPtr->latitude, dataPtr->latitudeSign,
                 dataPtr->longitude, dataPtr->longitudeSign );
  }
  else
  {
    DBGOUT( "GPS: Invalid\n" );
  }
  GPS_DATA_UNLOCK();
}

static void DATAMGMT_getMPU9250Data( void )
{
  T_MPU9250_OUT_DATA* dataPtr = NULL;

  MPU9250_DATA_LOCK();
  dataPtr = MPU9250_getPublicDataPtr();
  if( dataPtr->valid )
  {
    DBGOUT( "MPU: Y: %02f, P: %02f, R: %02f\n", dataPtr->yaw, dataPtr->pitch, dataPtr->roll );
  }
  else
  {
    DBGOUT( "MPU: Invalid\n" );
  }
  MPU9250_DATA_UNLOCK();
}
#endif

static void DATAMGMT_setState( T_DATAMGMT_ST state )
{
  if( dataMgmtCtrl.state != state )
  {
    //DBGOUT("DATAMGMT: st %u -> %u\n", dataMgmtCtrl.st, state );
    dataMgmtCtrl.state = state;
  }
}

//========EXPORTED FUNCTIONS DEFINITIONS========

void vTaskDataMgmt( void *p )
{
  uint8_t dataCount = 0;
  uint8_t rcvdChar = 0;

  while(1)
  {
#ifndef DATAMGMT_SENSOR_DEMO

    switch( dataMgmtCtrl.state )
    {
      case DATAMGMT_ST_IDLE:

        dataCount = UART_getRcvdDataCount( UART_PORT_CTRL ); //check if new data received from master
        if( dataCount == 0 )
        {
            UART_setTaskNotifications( UART_PORT_CTRL, true );
            ulTaskNotifyTake( pdTRUE, portMAX_DELAY );  // Wait until new data arrives
        }
        else
        {
            while( dataCount-- > 0)
            {
              UART_readData( UART_PORT_CTRL, &rcvdChar, 1 );
              if( rcvdChar == UART_CTRLPORT_FRAME_START ) //Start flag is not written to buffer
              {
                DATAMGMT_setState( DATAMGMT_ST_MSG_RECEIVING );
                break;
              }
            }
        }
        break;

      case DATAMGMT_ST_MSG_RECEIVING:
        //TODO: implement timeout
        dataCount = UART_getRcvdDataCount( UART_PORT_CTRL );
        while( dataCount-- > 0)
        {
          UART_readData( UART_PORT_CTRL, &rcvdChar , 1 );
          if( dataMgmtCtrl.signEscaped )
          {
            if( rcvdChar == UART_CTRLPORT_FRAME_END || rcvdChar == UART_CTRLPORT_FRAME_END || rcvdChar == UART_CTRLPORT_FRAME_ESC_SIGN )
            {
              // escaped special sign
              inputBuf[dataMgmtCtrl.rcvdCount++] = rcvdChar;
            }
            else
            {
              // no special sign following escape sign
              DBGOUT("DATAMGMT: False escape sign error!\n");
              DATAMGMT_setState( DATAMGMT_ST_ERROR );
            }
            dataMgmtCtrl.signEscaped = false;
          }
          else
          {
            switch( rcvdChar )
            {
              case UART_CTRLPORT_FRAME_START: //Framing error - unescaped start sign without previously receiving end sign
                DBGOUT("DATAMGMT: Unexpected frame start\n");
                DATAMGMT_setState( DATAMGMT_ST_ERROR );
                break;

              case UART_CTRLPORT_FRAME_END: //Frame finished
                DATAMGMT_setState( DATAMGMT_ST_MSG_VERIFY );
                break;

              case UART_CTRLPORT_FRAME_ESC_SIGN:  //Escape sign received
                dataMgmtCtrl.signEscaped = true;
                break;

              default:  //Regular data, write to buffer
                inputBuf[dataMgmtCtrl.rcvdCount++] = rcvdChar;
                break;
            }
          }
        }
        break;

      case DATAMGMT_ST_MSG_VERIFY:
        if( DATAMGMT_verifyInMessage( ( T_DATAMGMT_MSG_I* )&inputBuf ) )
        {
          DATAMGMT_setState( DATAMGMT_ST_CMD_EXECUTE );
        }
        else
        {
          DATAMGMT_setState( DATAMGMT_ST_ERROR );
        }
        break;

      case DATAMGMT_ST_CMD_EXECUTE:
        if ( DATAMGMT_dispatch( ( T_DATAMGMT_MSG_I* )&inputBuf ) )
        {
          DATAMGMT_setState( DATAMGMT_ST_TRANSMITTING );
        }
        else
        {
          DATAMGMT_setState( DATAMGMT_ST_ERROR );
        }
        break;

      case DATAMGMT_ST_TRANSMITTING:
        DATAMGMT_sendFrame( ( T_DATAMGMT_MSG_O* )&outputPrepBuf );
        DATAMGMT_setState( DATAMGMT_ST_DONE );
        break;

      case DATAMGMT_ST_DONE:
        dataMgmtCtrl.rcvdCount = 0;
        DATAMGMT_setState( DATAMGMT_ST_IDLE );
        UART_setTaskNotifications( UART_PORT_CTRL, true );
        break;

      case DATAMGMT_ST_ERROR:
        DBGOUT("DATAMGMT: Error state\n");
        dataMgmtCtrl.rcvdCount = 0;
        DATAMGMT_setState( DATAMGMT_ST_DONE );
        break;

      default:
        DBGOUT("DATAMGMT: unexpected state\n");
        ASSERT( NULL );
        DATAMGMT_setState( DATAMGMT_ST_ERROR );
        break;
    }

#else
  vTaskDelay( 1000 / portTICK_PERIOD_MS );

  DATAMGMT_getMPU9250Data();
  DATAMGMT_getGPSData();
  DATAMGMT_getEncoderData();
  DBGOUT("\n");
#endif  /* DATAMGMT_SENSOR_DEMO */

  }
}

void DATAMGMT_initialize( void )
{
  dataMgmtCtrl.state = DATAMGMT_ST_IDLE;
  DATAMGMT_initDataMgmtTask();
  ASSERT( dataMgmtCtrl.taskHndl );
  UART_registerTaskHndl( UART_PORT_CTRL, dataMgmtCtrl.taskHndl );
}

void DATAMGMT_triggerDataRead( void ) //TODO:remove
{
  dataMgmtCtrl.read = true;
}

TaskHandle_t* DATAMGMT_getTaskHndl( void )
{
  return (TaskHandle_t*)&dataMgmtCtrl.taskHndl;
}
