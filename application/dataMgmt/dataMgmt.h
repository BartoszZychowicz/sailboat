/**
 ******************************************************************************
 * @file    dataMgmt.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   dataMgmt header file.
 ******************************************************************************
 */

#ifndef DATAMGMT_DATAMGMT_H_
#define DATAMGMT_DATAMGMT_H_

typedef enum
{
  DATAMGMT_MODULE_MPU9250,
  DATAMGMT_MODULE_GPS,
  DATAMGMT_MODULE_ENCODER,
  DATAMGMT_MODULE_SERVO,
} T_DATAMGMT_MODULE;

void vTaskDataMgmt( void *p );

void DATAMGMT_initialize( void );

void DATAMGMT_triggerDataRead( void );

TaskHandle_t* DATAMGMT_getTaskHndl( void );

#endif /* DATAMGMT_DATAMGMT_H_ */
