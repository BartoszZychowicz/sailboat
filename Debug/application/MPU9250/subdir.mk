# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/MPU9250/MPU9250.c 

OBJS += \
./application/MPU9250/MPU9250.o 

C_DEPS += \
./application/MPU9250/MPU9250.d 


# Each subdirectory must supply rules for building sources it contributes
application/MPU9250/%.o: ../application/MPU9250/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


