# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/crc/crc8.c 

OBJS += \
./application/crc/crc8.o 

C_DEPS += \
./application/crc/crc8.d 


# Each subdirectory must supply rules for building sources it contributes
application/crc/%.o: ../application/crc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


