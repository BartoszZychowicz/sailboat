# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/dataMgmt/dataMgmt.c 

OBJS += \
./application/dataMgmt/dataMgmt.o 

C_DEPS += \
./application/dataMgmt/dataMgmt.d 


# Each subdirectory must supply rules for building sources it contributes
application/dataMgmt/%.o: ../application/dataMgmt/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


