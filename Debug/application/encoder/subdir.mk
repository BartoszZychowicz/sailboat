# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/encoder/encoder.c 

OBJS += \
./application/encoder/encoder.o 

C_DEPS += \
./application/encoder/encoder.d 


# Each subdirectory must supply rules for building sources it contributes
application/encoder/%.o: ../application/encoder/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


