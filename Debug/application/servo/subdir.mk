# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/servo/servo.c 

OBJS += \
./application/servo/servo.o 

C_DEPS += \
./application/servo/servo.d 


# Each subdirectory must supply rules for building sources it contributes
application/servo/%.o: ../application/servo/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


