# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/uart/uart.c \
../platform/uart/printf.c

OBJS += \
./platform/uart/uart.o \
./platform/uart/printf.o

C_DEPS += \
./platform/uart/uart.d \
./platform/uart/printf.d


# Each subdirectory must supply rules for building sources it contributes
platform/uart/%.o: ../platform/uart/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


