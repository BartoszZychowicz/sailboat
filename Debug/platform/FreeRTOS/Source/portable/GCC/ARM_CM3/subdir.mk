# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/FreeRTOS/Source/portable/GCC/ARM_CM3/port.c

OBJS += \
./platform/FreeRTOS/Source/portable/GCC/ARM_CM3/port.o

C_DEPS += \
./platform/FreeRTOS/Source/portable/GCC/ARM_CM3/port.d



# Each subdirectory must supply rules for building sources it contributes
platform/FreeRTOS/Source/portable/GCC/ARM_CM3/%.o: ../platform/FreeRTOS/Source/portable/GCC/ARM_CM3/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


