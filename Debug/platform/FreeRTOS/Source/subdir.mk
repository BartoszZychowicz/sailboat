# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/FreeRTOS/Source/croutine.c \
../platform/FreeRTOS/Source/event_groups.c \
../platform/FreeRTOS/Source/list.c \
../platform/FreeRTOS/Source/queue.c \
../platform/FreeRTOS/Source/tasks.c \
../platform/FreeRTOS/Source/timers.c

OBJS += \
./platform/FreeRTOS/Source/croutine.o \
./platform/FreeRTOS/Source/event_groups.o \
./platform/FreeRTOS/Source/list.o \
./platform/FreeRTOS/Source/queue.o \
./platform/FreeRTOS/Source/tasks.o \
./platform/FreeRTOS/Source/timers.o

C_DEPS += \
./platform/FreeRTOS/Source/croutine.d \
./platform/FreeRTOS/Source/event_groups.d \
./platform/FreeRTOS/Source/list.d \
./platform/FreeRTOS/Source/queue.d \
./platform/FreeRTOS/Source/tasks.d \
./platform/FreeRTOS/Source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
platform/FreeRTOS/Source/%.o: ../platform/FreeRTOS/Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


