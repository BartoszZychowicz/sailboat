# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/pwm/pwm.c 

OBJS += \
./platform/pwm/pwm.o 

C_DEPS += \
./platform/pwm/pwm.d 


# Each subdirectory must supply rules for building sources it contributes
platform/pwm/%.o: ../platform/pwm/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


