# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/rcc/rcc.c 

OBJS += \
./platform/rcc/rcc.o 

C_DEPS += \
./platform/rcc/rcc.d 


# Each subdirectory must supply rules for building sources it contributes
platform/rcc/%.o: ../platform/rcc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


