/**
 ******************************************************************************
 * @file    platHeaders.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Platform header file.
 ******************************************************************************
 */

#ifndef PLATHEADERS_H_
#define PLATHEADERS_H_

#include "stm32f1xx.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "portmacro.h"

#include "platCmn.h"
#include "./gpio/gpio.h"
#include "./i2c/i2c.h"
#include "./pwm/pwm.h"
#include "./rcc/rcc.h"
#include "./uart/uart.h"
#include "./uart/printf.h"


#endif /* PLATHEADERS_H_ */
