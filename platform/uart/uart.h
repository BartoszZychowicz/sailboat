/**
 ******************************************************************************
 * @file    uart.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   UART header file.
 ******************************************************************************
 */

#ifndef UART_UART_H_
#define UART_UART_H_

#define DBG_LOCK()   xSemaphoreTake( DBGMutex, portMAX_DELAY )
#define DBG_UNLOCK() xSemaphoreGive( DBGMutex )

#define UART_CTRLPORT_FRAME_START     0xE2  //TODO: move to datamgmt
#define UART_CTRLPORT_FRAME_END       0xE3
#define UART_CTRLPORT_FRAME_ESC_SIGN  0xF5

typedef enum
{
  UART_PORT_DBG,
  UART_PORT_GPS,
  UART_PORT_CTRL,
  UART_PORT_MAX
}T_UART_PORT;

extern SemaphoreHandle_t DBGMutex;

void UART_initialize( void );

void UART_disable( T_UART_PORT port );

void UART_enable( T_UART_PORT port );

void UART_sendData( T_UART_PORT port, uint8_t *data, uint32_t dataSize );

uint32_t UART_getRcvdDataCount( T_UART_PORT port );

void UART_readData( T_UART_PORT port, uint8_t *buf, uint32_t dataSize );

void UART_registerTaskHndl( T_UART_PORT port, TaskHandle_t taskHndl );

void UART_setTaskNotifications( T_UART_PORT port, bool state );

#endif /* UART_UART_H_ */




