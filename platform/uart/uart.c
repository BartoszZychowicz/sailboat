/**
 ******************************************************************************
 * @file    uart.c
 * @author 	Bartosz Zychowicz
 * @version
 * @date
 * @brief   Serial communication.
 ******************************************************************************
 */

//========INCLUDES========

#include "platHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...)             DBG_LOCK(); \
                                printf(__VA_ARGS__);    \
                                DBG_UNLOCK()
#define DBGOUT_ISR(...)         printf(__VA_ARGS__)     // Within ISRs use this, as attempt to LOCK would deadlock the system
#else
#define DBGOUT(...)
#define DBGOUT_ISR(...)
#endif

#define UART_DBG_RX_BUF_SIZE         64
#define UART_DBG_TX_BUF_SIZE        512
#define UART_GPS_RX_BUF_SIZE        256
#define UART_GPS_TX_BUF_SIZE          4
#define UART_CTRL_RX_BUF_SIZE        64
#define UART_CTRL_TX_BUF_SIZE       256

//========TYPES========

typedef struct
{
  volatile uint32_t     start;
  volatile uint32_t     end;
  volatile uint32_t     dataInBuf;
  const uint32_t        bufSize;
  const uint32_t        bufMask;
  const uint8_t         *buf;
} T_UART_BUF_CTRL;

typedef struct
{
  T_UART_BUF_CTRL       rxBufCtrl;
  T_UART_BUF_CTRL       txBufCtrl;
  UART_HandleTypeDef    hndl;
} T_UART_PORT_CTRL;


//========STATIC VARIABLES========

SemaphoreHandle_t DBGMutex;

static TaskHandle_t portTasks[UART_PORT_MAX] = { NULL };

static volatile bool taskNotificationsEnabled[UART_PORT_MAX] = { false };

static volatile uint8_t dbgRxBuf[ UART_DBG_RX_BUF_SIZE ] = {0};

static volatile uint8_t dbgTxBuf[ UART_DBG_TX_BUF_SIZE ] = {0};

static volatile uint8_t gpsRxBuf[ UART_GPS_RX_BUF_SIZE ] = {0};

static volatile uint8_t gpsTxBuf[ UART_GPS_TX_BUF_SIZE ] = {0};

static volatile uint8_t ctrlRxBuf[ UART_CTRL_RX_BUF_SIZE ] = {0};

static volatile uint8_t ctrlTxBuf[ UART_CTRL_TX_BUF_SIZE ] = {0};

//TODO: do not store bufsize, bufmask and buf ptr in RAM - constant values
static T_UART_PORT_CTRL UARTPorts[UART_PORT_MAX] =
{
  //DBG UART
  {
    .rxBufCtrl =
    {
      .start = 0,
      .end = 0,
      .dataInBuf = 0,
      .bufSize = ( UART_DBG_RX_BUF_SIZE - 1 ),
      .bufMask = ( UART_DBG_RX_BUF_SIZE - 1 ),
      .buf = (uint8_t*)&dbgRxBuf
    },
    .txBufCtrl =
    {
      .start = 0,
      .end = 0,
      .dataInBuf = 0,
      .bufSize = ( UART_DBG_TX_BUF_SIZE - 1 ),
      .bufMask = ( UART_DBG_TX_BUF_SIZE - 1 ),
      .buf = (uint8_t*)&dbgTxBuf
    }
  },
  //GPS UART
  {
    .rxBufCtrl =
    {
      .start = 0,
      .end = 0,
      .dataInBuf = 0,
      .bufSize = ( UART_GPS_RX_BUF_SIZE - 1 ),
      .bufMask = ( UART_GPS_RX_BUF_SIZE - 1 ),
      .buf = (uint8_t*)&gpsRxBuf
    },
    .txBufCtrl =
    {
      .start = 0,
      .end = 0,
      .dataInBuf = 0,
      .bufSize = ( UART_GPS_TX_BUF_SIZE - 1 ),
      .bufMask = ( UART_GPS_TX_BUF_SIZE - 1 ),
      .buf = (uint8_t*)&gpsTxBuf
    }
  },
  //CTRL UART
  {
    .rxBufCtrl =
    {
      .start = 0,
      .end = 0,
      .dataInBuf = 0,
      .bufSize = ( UART_CTRL_RX_BUF_SIZE - 1 ),
      .bufMask = ( UART_CTRL_RX_BUF_SIZE - 1 ),
      .buf = (uint8_t*)&ctrlRxBuf
    },
    .txBufCtrl =
    {
      .start = 0,
      .end = 0,
      .dataInBuf = 0,
      .bufSize = ( UART_CTRL_TX_BUF_SIZE - 1 ),
      .bufMask = ( UART_CTRL_TX_BUF_SIZE - 1 ),
      .buf = (uint8_t*)&ctrlTxBuf
    }
  }
};


//========STATIC FUNCTIONS PROTOTYPES========

static bool UART_storeDataInBuf( T_UART_BUF_CTRL *buf , uint8_t *data , uint32_t dataSize );

static bool UART_storeByteInBuf( T_UART_BUF_CTRL *buf , uint8_t *byte );

static bool UART_getDataFromBuf( T_UART_BUF_CTRL *buf , uint8_t *data , uint32_t dataSize );

static bool UART_getByteFromBuf( T_UART_BUF_CTRL *buf , uint8_t *byte );

static void UART_flushBuffer( T_UART_BUF_CTRL *buf );

//========STATIC FUNCTIONS DEFINITIONS========

static bool UART_storeDataInBuf( T_UART_BUF_CTRL *buf , uint8_t *data , uint32_t dataSize )
{
  bool res = true;

  if( ( buf->bufSize - buf->dataInBuf ) > dataSize )
  {
    uint32_t i = 0;
    for( ; i < dataSize; i++ )
    {
      if( UART_storeByteInBuf( buf, ( uint8_t* )( data + i ) ) )
      {
        buf->dataInBuf++;
      }
      else
      {
        DBGOUT_ISR("\nUART: Error writing data to buffer: buffer start = buffer end");
        res = false;
        break;
      }
    }
  }
  else
  {
    DBGOUT_ISR( "\nUART: Buffer overload!" );
    res = false;
  }

  return res;
}

static bool UART_storeByteInBuf( T_UART_BUF_CTRL *buf , uint8_t *byte )
{
  bool res = false;

  uint32_t tempBufStart = ( buf->start + 1 ) & buf->bufMask;

  if( tempBufStart != buf->end )
  {
    *( uint8_t* )( buf->buf + buf->start ) = *byte;
    buf->start = tempBufStart;
    res = true;
  }
  return res;
}


static bool UART_getDataFromBuf( T_UART_BUF_CTRL *buf , uint8_t *data , uint32_t dataSize )
{
  bool res = true;

  if( buf->dataInBuf >= dataSize )
  {
    uint32_t i = 0;
    for( ; i<dataSize; i++ )
    {
      if( UART_getByteFromBuf( buf, ( uint8_t* )( data + i ) ) )
      {
        buf->dataInBuf--;
      }
      else
      {
        res = false;
        break;
      }
    }
  }
  else
  {
    res = false;
  }

  return res;
}

static bool UART_getByteFromBuf( T_UART_BUF_CTRL *buf , uint8_t *byte )
{
  bool res = false;

  if( buf->end != buf->start )
  {
    *byte = *( uint8_t* )( buf->buf + buf->end );
    buf->end = ( buf->end + 1 ) & buf->bufMask;
    res = true;
  }

  return res;
}

static void UART_flushBuffer( T_UART_BUF_CTRL *buf )
{
  buf->start = 0;
  buf->end = 0;
  buf->dataInBuf = 0;
}

void USART1_IRQHandler( void )
{
  if( __HAL_UART_GET_IT_SOURCE( &( UARTPorts[UART_PORT_GPS].hndl ) , UART_IT_RXNE ) )
  {
    if( __HAL_UART_GET_FLAG( &(UARTPorts[UART_PORT_GPS].hndl), UART_FLAG_RXNE ) )
    {
      uint8_t dataRx = (uint8_t)(USART1->DR & 0x00FF );
      UART_storeDataInBuf( &(UARTPorts[UART_PORT_GPS].rxBufCtrl) , ( uint8_t* )&dataRx, sizeof( dataRx ) );
      if( taskNotificationsEnabled[UART_PORT_GPS] )
      {
        vTaskNotifyGiveFromISR( portTasks[UART_PORT_GPS], NULL);
        UART_setTaskNotifications( UART_PORT_GPS, false );
      }
    }
  }
  //TODO: remove TXE handling as long as GPS is 1-way communication
  if( __HAL_UART_GET_IT_SOURCE( &( UARTPorts[UART_PORT_GPS].hndl ) , UART_IT_TXE ) )
  {
    if( __HAL_UART_GET_FLAG( &(UARTPorts[UART_PORT_GPS].hndl), UART_FLAG_TXE ) )
    {
      uint8_t dataTx;
      if( UART_getDataFromBuf( &(UARTPorts[UART_PORT_GPS].txBufCtrl), ( uint8_t* )&dataTx, sizeof( dataTx ) ) )
      {
        USART1->DR = (uint32_t)(dataTx & 0x00FF);
      }
      else
      {
        __HAL_UART_DISABLE_IT( &(UARTPorts[UART_PORT_GPS].hndl), UART_IT_TXE );
      }
    }
  }
}

void USART2_IRQHandler( void )
{
  if( __HAL_UART_GET_IT_SOURCE( &( UARTPorts[UART_PORT_DBG].hndl ) , UART_IT_RXNE ) )
  {
    if( __HAL_UART_GET_FLAG( &(UARTPorts[UART_PORT_DBG].hndl), UART_FLAG_RXNE ) )
    {
      uint8_t dataRx = (uint8_t)(USART2->DR & 0x00FF );
      UART_storeDataInBuf( &(UARTPorts[UART_PORT_DBG].rxBufCtrl) , ( uint8_t* )&dataRx, sizeof( dataRx ) );
    }
  }

  if( __HAL_UART_GET_IT_SOURCE( &( UARTPorts[UART_PORT_DBG].hndl ) , UART_IT_TXE ) )
  {
    if( __HAL_UART_GET_FLAG( &(UARTPorts[UART_PORT_DBG].hndl), UART_FLAG_TXE ) )
    {
      uint8_t dataTx;
      if( UART_getDataFromBuf( &(UARTPorts[UART_PORT_DBG].txBufCtrl), ( uint8_t* )&dataTx, sizeof( dataTx ) ) )
      {
        USART2->DR = (uint32_t)(dataTx & 0x00FF);
      }
      else
      {
        __HAL_UART_DISABLE_IT( &(UARTPorts[UART_PORT_DBG].hndl), UART_IT_TXE );
      }
    }
  }
}

void USART3_IRQHandler( void )
{
  if( __HAL_UART_GET_IT_SOURCE( &( UARTPorts[UART_PORT_CTRL].hndl ) , UART_IT_RXNE ) )
  {
    if( __HAL_UART_GET_FLAG( &(UARTPorts[UART_PORT_CTRL].hndl), UART_FLAG_RXNE ) )
    {
      uint8_t dataRx = (uint8_t)(USART3->DR & 0x00FF );

      UART_storeDataInBuf( &(UARTPorts[UART_PORT_CTRL].rxBufCtrl) , ( uint8_t* )&dataRx, sizeof( dataRx ) );
      if( taskNotificationsEnabled[UART_PORT_CTRL] )
      {
        vTaskNotifyGiveFromISR( portTasks[UART_PORT_CTRL], NULL);
        UART_setTaskNotifications( UART_PORT_CTRL, false );
      }
    }
  }

  if( __HAL_UART_GET_IT_SOURCE( &( UARTPorts[UART_PORT_CTRL].hndl ) , UART_IT_TXE ) )
  {
    if( __HAL_UART_GET_FLAG( &(UARTPorts[UART_PORT_CTRL].hndl), UART_FLAG_TXE ) )
    {
      uint8_t dataTx;
      if( UART_getDataFromBuf( &(UARTPorts[UART_PORT_CTRL].txBufCtrl), ( uint8_t* )&dataTx, sizeof( dataTx ) ) )
      {
        USART3->DR = (uint32_t)(dataTx & 0x00FF);
      }
      else
      {
        __HAL_UART_DISABLE_IT( &(UARTPorts[UART_PORT_CTRL].hndl), UART_IT_TXE );
      }
    }
  }
}

//========EXPORTED FUNCTIONS DEFINITIONS========

void UART_initialize( void )
{
  //TODO: DBGMutex only in debug build
  DBGMutex = xSemaphoreCreateMutex();
  ASSERT( DBGMutex );

  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4); //TODO: move to a more suitable place

  //initialize UART_PORT_GPS on UART1
  RCC_enableClock( RCC_USART1_CLOCK );

  UARTPorts[UART_PORT_GPS].hndl.Instance = USART1;
  UARTPorts[UART_PORT_GPS].hndl.Init.BaudRate = 9600;
  UARTPorts[UART_PORT_GPS].hndl.Init.WordLength = UART_WORDLENGTH_8B;
  UARTPorts[UART_PORT_GPS].hndl.Init.Parity = UART_PARITY_NONE;
  UARTPorts[UART_PORT_GPS].hndl.Init.StopBits = UART_STOPBITS_1;
  UARTPorts[UART_PORT_GPS].hndl.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  UARTPorts[UART_PORT_GPS].hndl.Init.OverSampling = UART_OVERSAMPLING_16;
  UARTPorts[UART_PORT_GPS].hndl.Init.Mode = UART_MODE_TX_RX;

  HAL_UART_Init( &( UARTPorts[UART_PORT_GPS].hndl ) );

  HAL_NVIC_EnableIRQ( USART1_IRQn );
  HAL_NVIC_SetPriority(USART1_IRQn, 10, 10);
  __HAL_UART_ENABLE( &( UARTPorts[UART_PORT_GPS].hndl ) );

  __HAL_UART_ENABLE_IT( &( UARTPorts[UART_PORT_GPS].hndl ), UART_IT_RXNE );


  //initialize UART_PORT_DBG on UART2
  RCC_enableClock( RCC_USART2_CLOCK );

  UARTPorts[UART_PORT_DBG].hndl.Instance = USART2;
  UARTPorts[UART_PORT_DBG].hndl.Init.BaudRate = 115200;
  UARTPorts[UART_PORT_DBG].hndl.Init.WordLength = UART_WORDLENGTH_8B;
  UARTPorts[UART_PORT_DBG].hndl.Init.Parity = UART_PARITY_NONE;
  UARTPorts[UART_PORT_DBG].hndl.Init.StopBits = UART_STOPBITS_1;
  UARTPorts[UART_PORT_DBG].hndl.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  UARTPorts[UART_PORT_DBG].hndl.Init.OverSampling = UART_OVERSAMPLING_16;
  UARTPorts[UART_PORT_DBG].hndl.Init.Mode = UART_MODE_TX_RX;

  HAL_UART_Init( &( UARTPorts[UART_PORT_DBG].hndl ) );

  HAL_NVIC_EnableIRQ( USART2_IRQn );
  __HAL_UART_ENABLE( &( UARTPorts[UART_PORT_DBG].hndl ) );

  __HAL_UART_ENABLE_IT( &( UARTPorts[UART_PORT_DBG].hndl ), UART_IT_RXNE );


  //initialize UART_PORT_CTRL on UART3
  RCC_enableClock( RCC_USART3_CLOCK );

  UARTPorts[UART_PORT_CTRL].hndl.Instance = USART3;
  UARTPorts[UART_PORT_CTRL].hndl.Init.BaudRate = 115200;
  UARTPorts[UART_PORT_CTRL].hndl.Init.WordLength = UART_WORDLENGTH_8B;
  UARTPorts[UART_PORT_CTRL].hndl.Init.Parity = UART_PARITY_NONE;
  UARTPorts[UART_PORT_CTRL].hndl.Init.StopBits = UART_STOPBITS_1;
  UARTPorts[UART_PORT_CTRL].hndl.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  UARTPorts[UART_PORT_CTRL].hndl.Init.OverSampling = UART_OVERSAMPLING_16;
  UARTPorts[UART_PORT_CTRL].hndl.Init.Mode = UART_MODE_TX_RX;

  HAL_UART_Init( &( UARTPorts[UART_PORT_CTRL].hndl ) );

  HAL_NVIC_EnableIRQ( USART3_IRQn );
  HAL_NVIC_SetPriority(USART3_IRQn, 10, 10);  // lower priority set because it uses RTOS 'fromISR' API. TODO: verify
  __HAL_UART_ENABLE( &( UARTPorts[UART_PORT_CTRL].hndl ) );

  __HAL_UART_ENABLE_IT( &( UARTPorts[UART_PORT_CTRL].hndl ), UART_IT_RXNE );

}

void UART_disable( T_UART_PORT port )
{
  // Disable interrupts and UART peripheral
  __HAL_UART_DISABLE_IT( &( UARTPorts[port].hndl ), UART_IT_RXNE );
  __HAL_UART_DISABLE_IT( &( UARTPorts[port].hndl ), UART_IT_TXE );
  __HAL_UART_DISABLE( &( UARTPorts[port].hndl ) );
  // Flush related buffers
  UART_flushBuffer( &UARTPorts[port].rxBufCtrl );
  UART_flushBuffer( &UARTPorts[port].txBufCtrl );
}

void UART_enable( T_UART_PORT port )
{
  __HAL_UART_ENABLE( &( UARTPorts[port].hndl ) );
  __HAL_UART_ENABLE_IT( &( UARTPorts[port].hndl ), UART_IT_RXNE );
}

void UART_sendData( T_UART_PORT port, uint8_t *data, uint32_t dataSize )
{
  UART_storeDataInBuf( &(UARTPorts[port].txBufCtrl), data, dataSize );
  __HAL_UART_ENABLE_IT( &( UARTPorts[port].hndl ), UART_IT_TXE );
}

uint32_t UART_getRcvdDataCount( T_UART_PORT port )
{
  return UARTPorts[port].rxBufCtrl.dataInBuf;
}

void UART_readData( T_UART_PORT port, uint8_t *buf, uint32_t dataSize )
{
  UART_getDataFromBuf( &(UARTPorts[port].rxBufCtrl) , buf, dataSize );
}

void UART_registerTaskHndl( T_UART_PORT port, TaskHandle_t taskHndl )
{
  portTasks[port] = taskHndl;
}

void UART_setTaskNotifications( T_UART_PORT port, bool state )
{
  taskNotificationsEnabled[port] = state;
}

//TODO: add note about prototype in printf.h
void _putchar(char character)
{
  UART_sendData( UART_PORT_DBG, (uint8_t*)&character, sizeof( uint8_t ) );
}
