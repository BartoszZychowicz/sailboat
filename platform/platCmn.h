/**
 ******************************************************************************
 * @file    platCmn.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Platform common header file.
 ******************************************************************************
 */

#ifndef PLATCMN_H_
#define PLATCMN_H_

#define PLATCMN_ARRAY_MEMBER_COUNT( x ) ( sizeof( x ) / sizeof( x[0] ) )

#ifdef DEBUG
#define ASSERT(x)   if(!(x)){ \
                            printf("Assert failed file: %s, line %d\n\n", __FILE__, __LINE__); \
                            __disable_irq();}
#else
#define ASSERT(x)
#endif

#define PACKED __attribute__((packed))

// Define task stack sizes (in 4-byte words)
#define PLATCMN_STACK_SIZE_I2C1         256
#define PLATCMN_STACK_SIZE_MPU9250      256
#define PLATCMN_STACK_SIZE_ENCODER      256
#define PLATCMN_STACK_SIZE_GPS          512
#define PLATCMN_STACK_SIZE_DEBUG        256
#define PLATCMN_STACK_SIZE_DATAMGMT     256
#define PLATCMN_STACK_SIZE_HEARTBEAT    64

// Define common ASCII codes
#define PLATCMN_ASCII_LF    10
#define PLATCMN_ASCII_CR    13
#define PLATCMN_ASCII_ESC   27

#endif /* PLATCMN_H_ */
