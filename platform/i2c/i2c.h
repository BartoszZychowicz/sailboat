/**
 ******************************************************************************
 * @file    i2c.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   I2C header file.
 ******************************************************************************
 */

#ifndef I2C_I2C_H_
#define I2C_I2C_H_

#define I2C1_LOCK()   xSemaphoreTake( I2C1Mutex, portMAX_DELAY )
#define I2C1_UNLOCK() xSemaphoreGive( I2C1Mutex )

typedef enum
{
  I2C_PORT_1,
  I2C_PORT_MAX
}T_I2C_PORT;

extern SemaphoreHandle_t I2C1Mutex;

void vTaskI2C1(void *p);

void I2C_initialize( void );

void I2C_readData( T_I2C_PORT port, uint8_t devAddr, uint8_t regAddr, uint8_t dataSize, uint8_t *data, TaskHandle_t submitter, HAL_StatusTypeDef *result );

void I2C_writeData( T_I2C_PORT port, uint8_t devAddr, uint8_t regAddr, uint8_t dataSize, uint8_t *data, TaskHandle_t submitter, HAL_StatusTypeDef *result );

TaskHandle_t* I2C_getTask1Hndl( void );

#endif /* I2C_I2C_H_ */
