/**
 ******************************************************************************
 * @file    i2c.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   I2C driver.
 ******************************************************************************
 */

//========INCLUDES========

#include "platHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#define I2C_TIMEOUT 100

//========TYPES========

typedef enum
{
  I2C_DIR_WRITE,
  I2C_DIR_READ
} T_I2C_DIR;

typedef struct
{
  uint8_t                 devAddr;
  uint8_t                 regAddr;
  uint8_t                 dataSize;
  uint8_t                 *data;
  TaskHandle_t            submitter;
  T_I2C_DIR               dir;
  HAL_StatusTypeDef       *result;
} T_I2C_REQUEST;

typedef struct
{
  T_I2C_REQUEST           req;
  TaskHandle_t            taskHndl;
  I2C_HandleTypeDef       hndl;
} T_I2C_PORT_CTRL;


//========STATIC VARIABLES========

static T_I2C_PORT_CTRL I2CPorts[I2C_PORT_MAX];

//========EXPORTED VARIABLES========

SemaphoreHandle_t I2C1Mutex;

//========STATIC FUNCTIONS PROTOTYPES========

static void I2C_initI2C1Task( void );

//========STATIC FUNCTIONS DEFINITIONS========

static void I2C_initI2C1Task( void )
{
  xTaskCreate( vTaskI2C1, (const char*) "I2C1Task", PLATCMN_STACK_SIZE_I2C1, NULL, 1, &I2CPorts[I2C_PORT_1].taskHndl );
}

//========EXPORTED FUNCTIONS DEFINITIONS========

void vTaskI2C1(void *p)
{

  while(1)
  {
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY ); // wait indefinitely for request to perform;

    if( I2CPorts[I2C_PORT_1].req.dir == I2C_DIR_READ )
    {
      *I2CPorts[I2C_PORT_1].req.result = HAL_I2C_Mem_Read( &I2CPorts[I2C_PORT_1].hndl, I2CPorts[I2C_PORT_1].req.devAddr,
                                                I2CPorts[I2C_PORT_1].req.regAddr, I2C_MEMADD_SIZE_8BIT,
                                                I2CPorts[I2C_PORT_1].req.data, I2CPorts[I2C_PORT_1].req.dataSize,
                                                I2C_TIMEOUT );
    }
    else
    {
      *I2CPorts[I2C_PORT_1].req.result = HAL_I2C_Mem_Write( &I2CPorts[I2C_PORT_1].hndl, I2CPorts[I2C_PORT_1].req.devAddr,
                                                 I2CPorts[I2C_PORT_1].req.regAddr, I2C_MEMADD_SIZE_8BIT,
                                                 I2CPorts[I2C_PORT_1].req.data, I2CPorts[I2C_PORT_1].req.dataSize,
                                                 I2C_TIMEOUT );
    }

    xTaskNotifyGive( I2CPorts[I2C_PORT_1].req.submitter );  // Notify request submitter task
  }

}

void I2C_initialize( void )
{
  RCC_enableClock( RCC_I2C1_CLOCK );

  I2CPorts[I2C_PORT_1].hndl.Instance             = I2C1;
  I2CPorts[I2C_PORT_1].hndl.Init.ClockSpeed      = 100000;
  I2CPorts[I2C_PORT_1].hndl.Init.DutyCycle       = I2C_DUTYCYCLE_2;
  I2CPorts[I2C_PORT_1].hndl.Init.OwnAddress1     = 0xFF;
  I2CPorts[I2C_PORT_1].hndl.Init.AddressingMode  = I2C_ADDRESSINGMODE_7BIT;
  I2CPorts[I2C_PORT_1].hndl.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  I2CPorts[I2C_PORT_1].hndl.Init.OwnAddress2     = 0xFF;
  I2CPorts[I2C_PORT_1].hndl.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  I2CPorts[I2C_PORT_1].hndl.Init.NoStretchMode   = I2C_NOSTRETCH_DISABLE;

  HAL_I2C_Init( &( I2CPorts[I2C_PORT_1].hndl ) );
  __HAL_I2C_ENABLE( &(I2CPorts[I2C_PORT_1].hndl) );

  I2C1Mutex = xSemaphoreCreateMutex();
  ASSERT(I2C1Mutex);

  I2C_initI2C1Task();
  ASSERT(I2CPorts[I2C_PORT_1].taskHndl);
}

void I2C_readData( T_I2C_PORT port, uint8_t devAddr, uint8_t regAddr, uint8_t dataSize, uint8_t *data, TaskHandle_t submitter, HAL_StatusTypeDef *result )
{
    I2CPorts[port].req.devAddr = devAddr;
    I2CPorts[port].req.regAddr = regAddr;
    I2CPorts[port].req.dataSize = dataSize;
    I2CPorts[port].req.data = data;
    I2CPorts[port].req.submitter = submitter;
    I2CPorts[port].req.result = result;
    I2CPorts[port].req.dir = I2C_DIR_READ;

    xTaskNotifyGive( I2CPorts[port].taskHndl );    // Notify I2C task request needs to be handled
}

void I2C_writeData( T_I2C_PORT port, uint8_t devAddr, uint8_t regAddr, uint8_t dataSize, uint8_t *data, TaskHandle_t submitter, HAL_StatusTypeDef *result )
{
  I2CPorts[port].req.devAddr = devAddr;
  I2CPorts[port].req.regAddr = regAddr;
  I2CPorts[port].req.dataSize = dataSize;
  I2CPorts[port].req.data = data;
  I2CPorts[port].req.submitter = submitter;
  I2CPorts[port].req.result = result;
  I2CPorts[port].req.dir = I2C_DIR_WRITE;

  xTaskNotifyGive( I2CPorts[port].taskHndl );    // Notify I2C task request needs to be handled
}

TaskHandle_t* I2C_getTask1Hndl( void )
{
  return ( TaskHandle_t* )&I2CPorts[I2C_PORT_1].taskHndl;
}
