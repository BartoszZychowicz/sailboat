/**
 ******************************************************************************
 * @file    rcc.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   Reset and clock module.
 ******************************************************************************
 */

//========INCLUDES========

#include "platHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#define RCC_ENABLE_CLOCK_GPIOA    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPAEN)
#define RCC_ENABLE_CLOCK_GPIOB    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPBEN)
#define RCC_ENABLE_CLOCK_GPIOC    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPCEN)
#define RCC_ENABLE_CLOCK_GPIOD    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPDEN)

#define RCC_ENABLE_CLOCK_USART1   SET_BIT(RCC->APB2ENR, RCC_APB2ENR_USART1EN)
#define RCC_ENABLE_CLOCK_USART2   SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART2EN)
#define RCC_ENABLE_CLOCK_USART3   SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN)

#define RCC_ENABLE_CLOCK_I2C1     SET_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C1EN)

#define RCC_ENABLE_CLOCK_TIM4     SET_BIT(RCC->APB1ENR, RCC_APB1ENR_TIM4EN)

//========TYPES========

//========STATIC VARIABLES========

//========STATIC FUNCTIONS PROTOTYPES========

//========STATIC FUNCTIONS DEFINITIONS========

//========EXPORTED FUNCTIONS DEFINITIONS========

void RCC_enableClock( T_RCC_MODULE_CLOCK clock )
{
  switch( clock )
  {
    case RCC_GPIOA_CLOCK:
      RCC_ENABLE_CLOCK_GPIOA;
      break;

    case RCC_GPIOB_CLOCK:
      RCC_ENABLE_CLOCK_GPIOB;
      break;

    case RCC_GPIOC_CLOCK:
      RCC_ENABLE_CLOCK_GPIOC;
      break;

    case RCC_GPIOD_CLOCK:
      RCC_ENABLE_CLOCK_GPIOD;
      break;

    case RCC_USART1_CLOCK:
      RCC_ENABLE_CLOCK_USART1;
      break;

    case RCC_USART2_CLOCK:
      RCC_ENABLE_CLOCK_USART2;
      break;

    case RCC_USART3_CLOCK:
      RCC_ENABLE_CLOCK_USART3;
      break;

    case RCC_I2C1_CLOCK:
      RCC_ENABLE_CLOCK_I2C1;
      break;

    case RCC_TIM4_CLOCK:
      RCC_ENABLE_CLOCK_TIM4;
      break;

    default:
      ASSERT(NULL);
      break;
  }
}



bool RCC_initSystemClock( void )
{

  RCC_OscInitTypeDef oscInit;

  oscInit.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  oscInit.HSIState = RCC_HSI_ON;

  oscInit.PLL.PLLState = RCC_PLL_ON;
  oscInit.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  oscInit.PLL.PLLMUL = RCC_PLL_MUL16;

  HAL_StatusTypeDef status = HAL_RCC_OscConfig(&oscInit);

  if(status){
    return false;
  }

  RCC_ClkInitTypeDef clkInit;

  clkInit.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  clkInit.SYSCLKSource = RCC_CFGR_SW_PLL;
  clkInit.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkInit.APB1CLKDivider = RCC_HCLK_DIV2;
  clkInit.APB2CLKDivider = RCC_HCLK_DIV1;

  status |= HAL_RCC_ClockConfig(&clkInit, FLASH_LATENCY_2);
  if(status){
    return false;
  }

  SystemCoreClockUpdate();
  return true;      //TODO: only 1 return

}
