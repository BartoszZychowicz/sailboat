/**
 ******************************************************************************
 * @file    rcc.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   RCC header file.
 ******************************************************************************
 */

#ifndef RCC_RCC_H_
#define RCC_RCC_H_

typedef enum
{
  RCC_GPIOA_CLOCK,
  RCC_GPIOB_CLOCK,
  RCC_GPIOC_CLOCK,
  RCC_GPIOD_CLOCK,
  RCC_USART1_CLOCK,
  RCC_USART2_CLOCK,
  RCC_USART3_CLOCK,
  RCC_I2C1_CLOCK,
  RCC_TIM4_CLOCK
} T_RCC_MODULE_CLOCK;


void RCC_enableClock( T_RCC_MODULE_CLOCK clock );

bool RCC_initSystemClock( void );

#endif /* RCC_RCC_H_ */
