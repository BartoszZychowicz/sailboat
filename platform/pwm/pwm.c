/**
 ******************************************************************************
 * @file    pwm.c
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   PWM driver.
 ******************************************************************************
 */

#include "platHeaders.h"

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#define PWM_PRESCALED_CLOCK 100000  // Timer clock frequency
#define PWM_TIM_FREQ  100           // Desired timer reload frequency
#define PWM_CHANNELS_USED 2         // Amount of PWM channels used
#define PWM_TIM4_CHAN_3_INIT 144
#define PWM_TIM4_CHAN_4_INIT 123

typedef struct
{
    TIM_HandleTypeDef* timHndl;
    uint32_t channel;
    uint32_t compValue;
} T_PWM_CHNL_LIST;

typedef struct
{
    TIM_HandleTypeDef hndl;
} T_PWM_TIM_CTRL;

static T_PWM_TIM_CTRL timCtrl;

static T_PWM_CHNL_LIST chnlList[PWM_CHANNELS_USED];

void PWM_initialize(void)
{
  uint8_t i = 0;

  RCC_enableClock( RCC_TIM4_CLOCK );

  // Initialize timer4
  timCtrl.hndl.Instance = TIM4;
  timCtrl.hndl.Init.Period = ( PWM_PRESCALED_CLOCK / PWM_TIM_FREQ ) - 1;        // 10ms period
  timCtrl.hndl.Init.Prescaler = ( SystemCoreClock / PWM_PRESCALED_CLOCK ) - 1;  // Scale system clock to desired value
  timCtrl.hndl.Init.ClockDivision = 0;
  timCtrl.hndl.Init.CounterMode = TIM_COUNTERMODE_UP;
  timCtrl.hndl.Init.RepetitionCounter = 0;

  HAL_TIM_PWM_Init( &timCtrl.hndl );

  // Initialize PWM channels

  chnlList[PWM_CHNL_TIM4_CHNL3].timHndl = &timCtrl.hndl;
  chnlList[PWM_CHNL_TIM4_CHNL3].channel = TIM_CHANNEL_3;
  chnlList[PWM_CHNL_TIM4_CHNL3].compValue = PWM_TIM4_CHAN_3_INIT;

  chnlList[PWM_CHNL_TIM4_CHNL4].timHndl = &timCtrl.hndl;
  chnlList[PWM_CHNL_TIM4_CHNL4].channel = TIM_CHANNEL_4;
  chnlList[PWM_CHNL_TIM4_CHNL4].compValue = PWM_TIM4_CHAN_4_INIT;

  TIM_OC_InitTypeDef ocStruct;                //output compare structure
  ocStruct.OCMode = TIM_OCMODE_PWM1;
  ocStruct.OCPolarity = TIM_OCPOLARITY_HIGH;
  ocStruct.OCNPolarity = TIM_OCNPOLARITY_LOW;
  ocStruct.OCFastMode = TIM_OCFAST_ENABLE;
  ocStruct.OCIdleState = TIM_OCIDLESTATE_SET;
  ocStruct.OCNIdleState = TIM_OCNIDLESTATE_RESET;

  for( i = 0; i < PWM_CHANNELS_USED; i++ )
  {
    ocStruct.Pulse = chnlList[i].compValue;
    HAL_TIM_PWM_ConfigChannel( chnlList[i].timHndl, &ocStruct, chnlList[i].channel );
    HAL_TIM_PWM_Start( chnlList[i].timHndl, chnlList[i].channel );
  }
}

void PWM_setCompValue( T_PWM_CHNL chnl, uint32_t value )
{
  if( chnl < PWM_CHNL_MAX )
  {
    chnlList[chnl].compValue = value;
    __HAL_TIM_SET_COMPARE( chnlList[chnl].timHndl, chnlList[chnl].channel, chnlList[chnl].compValue );
  }
}
