/**
 ******************************************************************************
 * @file    pwm.h
 * @author  Bartosz Zychowicz
 * @version
 * @date
 * @brief   PWM header file.
 ******************************************************************************
 */

#ifndef PWM_PWM_H_
#define PWM_PWM_H_

// Keep in sync with chnlList array in pwm.c
typedef enum
{
  PWM_CHNL_TIM4_CHNL3,
  PWM_CHNL_TIM4_CHNL4,
  PWM_CHNL_MAX
} T_PWM_CHNL;

void PWM_initialize( void );

void PWM_setCompValue( T_PWM_CHNL chnl, uint32_t value );

#endif /* PWM_PWM_H_ */
