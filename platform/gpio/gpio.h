/**
 ******************************************************************************
 * @file    gpio.h
 * @author 	Bartosz Zychowicz
 * @version
 * @date
 * @brief   GPIO header file.
 ******************************************************************************
 */

#ifndef GPIO_GPIO_H_
#define GPIO_GPIO_H_

void vTaskHeartbeat( void *p );

void GPIO_initialize( void );

TaskHandle_t* GPIO_getHeartbeatTaskHndl( void );

#endif /* GPIO_GPIO_H_ */
