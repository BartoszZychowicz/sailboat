/**
 ******************************************************************************
 * @file    gpio.c
 * @author 	Bartosz Zychowicz
 * @version
 * @date
 * @brief   General purpose inputs/outputs.
 ******************************************************************************
 */


//========INCLUDES========

#include "platHeaders.h"

//========DEFINES========

#ifdef DEBUG
#define DBGOUT(...) DBG_LOCK(); \
                    printf(__VA_ARGS__);  \
                    DBG_UNLOCK()
#else
#define DBGOUT(...)
#endif

#define GPIO_HEARTBEAT_PERIOD   500   //!< Heartbeat LED period in ms.

//========TYPES========

// WARNING - pins here should be exactly the same and in the same order as in gpioConfig struct.
typedef enum
{
  GPIO_PIN_LED_HB,
  GPIO_PIN_USART1_TX,
  GPIO_PIN_USART1_RX,
  GPIO_PIN_USART2_TX,
  GPIO_PIN_USART2_RX,
  GPIO_PIN_USART3_TX,
  GPIO_PIN_USART3_RX,
  GPIO_PIN_I2C1_SCL,
  GPIO_PIN_I2C1_SDA,
  GPIO_PIN_TIM4_PWM_CHNL_3,
  GPIO_PIN_TIM4_PWM_CHNL_4,
  GPIO_PIN_MAX
} T_GPIO_PIN;

typedef struct
{
  GPIO_TypeDef *port;
  GPIO_InitTypeDef params;
} T_GPIO_PIN_CONFIG;

typedef struct
{
  GPIO_TypeDef        *port;
  T_RCC_MODULE_CLOCK  clockName;
} T_GPIO_PORTLIST;

//========STATIC VARIABLES========

static TaskHandle_t heartbeatTaskHndl;

static const T_GPIO_PORTLIST portList[] =
{
    { GPIOA, RCC_GPIOA_CLOCK },
    { GPIOB, RCC_GPIOB_CLOCK },
    { GPIOC, RCC_GPIOC_CLOCK },
    { GPIOD, RCC_GPIOD_CLOCK }
};

// set used pins and their configuration here.
// WARNING - pins here should be exactly the same and in the same order as in T_GPIO_PIN enumerator.
static const T_GPIO_PIN_CONFIG gpioConfig[GPIO_PIN_MAX] =
{
    [GPIO_PIN_LED_HB]           = { GPIOA, { GPIO_PIN_5,   GPIO_MODE_OUTPUT_PP,  GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_USART1_TX]        = { GPIOA, { GPIO_PIN_9,   GPIO_MODE_AF_PP,      GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_USART1_RX]        = { GPIOA, { GPIO_PIN_10,  GPIO_MODE_AF_INPUT,   GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_USART2_TX]        = { GPIOA, { GPIO_PIN_2,   GPIO_MODE_AF_PP,      GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_USART2_RX]        = { GPIOA, { GPIO_PIN_3,   GPIO_MODE_AF_INPUT,   GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_USART3_TX]        = { GPIOB, { GPIO_PIN_10,  GPIO_MODE_AF_PP,      GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_USART3_RX]        = { GPIOB, { GPIO_PIN_11,  GPIO_MODE_AF_INPUT,   GPIO_NOPULL, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_I2C1_SCL]         = { GPIOB, { GPIO_PIN_6,   GPIO_MODE_AF_OD,      GPIO_PULLUP, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_I2C1_SDA]         = { GPIOB, { GPIO_PIN_7,   GPIO_MODE_AF_OD,      GPIO_PULLUP, GPIO_SPEED_FREQ_LOW   } },
    [GPIO_PIN_TIM4_PWM_CHNL_3]  = { GPIOB, { GPIO_PIN_8,   GPIO_MODE_AF_PP,      GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH  } },
    [GPIO_PIN_TIM4_PWM_CHNL_4]  = { GPIOB, { GPIO_PIN_9,   GPIO_MODE_AF_PP,      GPIO_NOPULL, GPIO_SPEED_FREQ_HIGH  } }
};

//========STATIC FUNCTIONS PROTOTYPES========

static void GPIO_initHeartbeatTask( void );

static bool GPIO_checkIfPortInUse( const GPIO_TypeDef *port );

static void GPIO_enableClocks( void );

//========STATIC FUNCTIONS DEFINITIONS========

static void GPIO_initHeartbeatTask( void )
{
  xTaskCreate( vTaskHeartbeat, (const char*) "HeartbeatTask", PLATCMN_STACK_SIZE_HEARTBEAT, NULL, 1, &heartbeatTaskHndl );
}

static bool GPIO_checkIfPortInUse( const GPIO_TypeDef *port )
{
  bool result = false;
  uint16_t i = 0;
  for( ; i < GPIO_PIN_MAX; i++)
  {
    if( gpioConfig[i].port ==  port )
    {
      result = true;
      break;
    }
  }
  return result;
}



static void GPIO_enableClocks( void )
{
  uint8_t i = 0;
  for( ; i < PLATCMN_ARRAY_MEMBER_COUNT(portList); i++ )
  {
    if ( GPIO_checkIfPortInUse( portList[i].port ) )
    {
      RCC_enableClock( portList[i].clockName );
    }
  }
}

//========EXPORTED FUNCTIONS DEFINITIONS========

void vTaskHeartbeat( void *p )
{
  while( 1 )
  {
    HAL_GPIO_TogglePin( GPIOA , GPIO_PIN_5 );
    vTaskDelay( GPIO_HEARTBEAT_PERIOD / portTICK_PERIOD_MS );
  }
}

void GPIO_initialize( void )
{
  GPIO_enableClocks();

  for(uint16_t i = 0; i < GPIO_PIN_MAX; i++)
  {
    HAL_GPIO_Init( gpioConfig[i].port, ( GPIO_InitTypeDef* )&gpioConfig[i].params );
  }

  GPIO_initHeartbeatTask();
  ASSERT( heartbeatTaskHndl );
}

TaskHandle_t* GPIO_getHeartbeatTaskHndl( void )
{
  return (TaskHandle_t*)&heartbeatTaskHndl;
}
