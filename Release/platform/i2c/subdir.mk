# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/i2c/i2c.c 

OBJS += \
./platform/i2c/i2c.o  

C_DEPS += \
./platform/i2c/i2c.d 


# Each subdirectory must supply rules for building sources it contributes
platform/i2c/%.o: ../platform/i2c/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


