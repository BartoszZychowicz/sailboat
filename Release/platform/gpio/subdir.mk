# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/gpio/gpio.c 

OBJS += \
./platform/gpio/gpio.o  

C_DEPS += \
./platform/gpio/gpio.d 


# Each subdirectory must supply rules for building sources it contributes
platform/gpio/%.o: ../platform/gpio/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


