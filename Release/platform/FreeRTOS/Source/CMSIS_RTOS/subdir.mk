# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c

OBJS += \
./platform/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.o

C_DEPS += \
./platform/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.d



# Each subdirectory must supply rules for building sources it contributes
platform/FreeRTOS/Source/CMSIS_RTOS/%.o: ../platform/FreeRTOS/Source/CMSIS_RTOS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


