# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../platform/system/stm32f1xx_it.c \
../platform/system/syscalls.c \
../platform/system/system_stm32f1xx.c

OBJS += \
./platform/system/stm32f1xx_it.o \
./platform/system/syscalls.o \
./platform/system/system_stm32f1xx.o

C_DEPS += \
./platform/system/stm32f1xx_it.d \
./platform/system/syscalls.d \
./platform/system/system_stm32f1xx.d


# Each subdirectory must supply rules for building sources it contributes
platform/system/%.o: ../platform/system/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


