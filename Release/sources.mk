OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
application \
application/crc \
application/dataMgmt \
application/debug \
application/encoder \
application/GPS \
application/MPU9250 \
application/servo \
HAL_Driver/Src \
startup \
platform/FreeRTOS/Source/CMSIS_RTOS \
platform/FreeRTOS/Source \
platform/FreeRTOS/Source/portable/GCC/ARM_CM3 \
platform/FreeRTOS/Source/portable/MemMang \
platform/gpio \
platform/i2c \
platform/rcc \
platform/system \
platform/uart
