# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/debug/debug.c 

OBJS += \
./application/debug/debug.o  

C_DEPS += \
./application/debug/debug.d 


# Each subdirectory must supply rules for building sources it contributes
application/debug/%.o: ../application/debug/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


