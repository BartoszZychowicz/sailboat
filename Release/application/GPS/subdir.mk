# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/GPS/GPS.c 

OBJS += \
./application/GPS/GPS.o  

C_DEPS += \
./application/GPS/GPS.d 


# Each subdirectory must supply rules for building sources it contributes
application/GPS/%.o: ../application/GPS/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


