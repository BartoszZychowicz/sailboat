# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/main.c

OBJS += \
./application/main.o

C_DEPS += \
./application/main.d


# Each subdirectory must supply rules for building sources it contributes
application/%.o: ../application/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	$(C_CMD) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


