#Autonomous sailboat master-slave protocol decoder
#Version: 1.00
#Author: Bartosz Zychowicz

#imports
import crc8
import struct

#constants
START_FLAG = 'E2'
END_FLAG = 'E3'
ESCAPE_SIGN = 'F5'

FRAMETYPE_ACK = '10'
FRAMETYPE_BASIC_SENSOR = '11'

FRAMETYPE_REQ_BASIC_SENSOR = '60'
FRAMETYPE_SET_SERVOS = '61'


#global variables
flags = [START_FLAG, END_FLAG, ESCAPE_SIGN]


#Function definitions

#Check if given byte is a special flag
#Return 1 if true, 0 if false
def isFlag(char):
    if char in flags:
        return 1
    else:
        return 0
    

#Validate input string - hex format and correct start and end flags
#Return 1 if valid, 0 if invalid
def validateInput(inString):

    error = 0
    
    for i in inString:
        #check bytes len
        if len(i) != 2:
            print('input error - all bytes must be given as 2 ASCII chars')
            error = 1
            break;
        #check if input is hex
        try:
            temp = int(i, 16)
        except ValueError:
            print('input error - only 0-9 and A-F chars allowed')
            error = 1
            break;

    first = inString[0]
    last = inString[len(inString) - 1]

    if first != "E2":
        print('input error - incorrect start flag 0x{}. Should be 0xE2'.format(first))
        error = 1    
    if last != "E3":
        print('input error - incorrect end flag 0x{}. Should be 0xE3'.format(last))
        error = 1

    if error == 1:
        return 0
    else:
        return 1


#Remove special signs from frame
def removeFlags(frame):
    i = 0
    while i < len(frame):
        if frame[i] == ESCAPE_SIGN: 
            if isFlag(frame[i+1]):  #escape sign before special sign - remove it
                del frame[i]
                i = i + 1
        i = i + 1

    #remove start and end flags
    del frame[0]
    del frame[len(frame) - 1]


#verify frame length and CRC
def verifyFrame(frame):
    
    realFrameLen = len(frame)
    declaredFrameLen = int(frame[1], 16)

    #verify frame length
    if realFrameLen != (declaredFrameLen + 3):
        print('Payload len {} not equal to declared in length field {}'.format(realFrameLen-3, declaredFrameLen))
        return 0

    #verify CRC
    declaredCRC = frame.pop(realFrameLen-1)
    hash = crc8.crc8()
    
    for i in frame:
        hash.update(bytearray.fromhex(i))
        
    calcCRC = (hash.hexdigest()).upper()

    if calcCRC != declaredCRC:
        print("Calculated CRC 0x{} not equal to received 0x{}".format(calcCRC, declaredCRC))
        return 0

    return 1

#Check frame type and parse it with appropriate function
def dispatchFrame(frame):
   
    frameType = frame[0]
    if(len(frame) > 2):
        framePayload = frame[2:(len(frame))]  #omit frame type and len bytes

    if frameType == FRAMETYPE_ACK:
        print('ACK frame')

    elif frameType == FRAMETYPE_BASIC_SENSOR:
        print('Basic sensor data frame:')
        parseBasicSensor(framePayload)

    elif frameType == FRAMETYPE_REQ_BASIC_SENSOR:
        print('Request basic sensor data frame')

    elif frameType == FRAMETYPE_SET_SERVOS:
        print('Set servos frame:')
        parseSetServos(framePayload)

#Parse basic sensor data frame
def parseBasicSensor(frame):

    #convert strings to byte arrays
    Yaw = bytearray.fromhex(''.join(frame[0:4]))
    Pitch = bytearray.fromhex(''.join(frame[4:8]))
    Roll = bytearray.fromhex(''.join(frame[8:12]))
    Longitude = bytearray.fromhex(''.join(frame[12:16]))
    Latitude = bytearray.fromhex(''.join(frame[16:20]))
    Encoder = bytearray.fromhex(''.join(frame[20:24]))
    infoMask = bytearray.fromhex(frame[24])

    #convert byte arrays to floats
    YawFloat = struct.unpack('f', Yaw)
    PitchFloat = struct.unpack('f', Pitch)
    RollFloat = struct.unpack('f', Roll)
    LongitudeFloat = struct.unpack('f', Longitude)
    LatitudeFloat = struct.unpack('f', Latitude)
    EncoderFloat = struct.unpack('f', Encoder)
   
    LongitudeSign = 'N' if ( infoMask[0] & 0x01 ) else 'S'
    LatitudeSign = 'W' if ( infoMask[0] & 0x02 ) else 'E'
    MPURes = 'OK' if ( infoMask[0] & 0x04 ) else 'Fail'
    GPSRes = 'OK' if ( infoMask[0] & 0x08 ) else 'Fail'
    EncoderRes = 'OK' if ( infoMask[0] & 0x10 ) else 'Fail'

    print('9DoF {} : Yaw {:.3f}, Pitch {:.3f}, Roll {:.3f}'.format(MPURes, YawFloat[0], PitchFloat[0], RollFloat[0]))
    print('GPS {} : Longitude {:.3f}{}, Latitude {:.3f}{}'.format(GPSRes, LongitudeFloat[0], LongitudeSign, LatitudeFloat[0], LatitudeSign))
    print('Encoder {} : Angle {:.3f}'.format(EncoderRes, EncoderFloat[0]))
    

#Parse set servos frame
def parseSetServos(frame):
    try:
        sail = int(frame[0], 16)
        rudder = int(frame[1], 16)
    except ValueError:
        print('Invalid parameter')
        return 0
    
    print('Set sail to {}, rudder to {}'.format(sail, rudder))
    return 1

#Main function
def main():

    print('@@@ Autonomous sailboat master-slave protocol decoder @@@')
    print('Author: Bartosz Zychowicz')
    print('Frames should be provided as space-separated ASCII strings for each byte')
    print('Example: E2 60 00 F5 F5 E3\n')
    
    while(1):
        error = 0

        #Get input 
        input_string = input('Provide a frame to decode:')

        #Convert to uppercase list
        input_string = input_string.upper()
        inTab = input_string.rsplit(" ")

        #verify input format and flags
        res = validateInput(inTab)
        if res != 1:
            continue;

        #remove byte stuffing
        removeFlags(inTab)
        #verify length and CRC
        res = verifyFrame(inTab)
        if res != 1:
            continue;

        dispatchFrame(inTab)
        print('\n')

main()
    

